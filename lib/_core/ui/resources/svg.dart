part of resources;

class _Assets {
  static _Assets _instance = _Assets._();

  const _Assets._();

  factory _Assets() => _instance;

  static const String assetDir = "assets/images";
  static const String dirSvg = "$assetDir/svg/";
  static const String dirImage = "$assetDir/png/";

  static MapEntry<String, SvgPicture> svg(
    String name, {
    Color? color,
    String? dir,
    String? key,
    double? size,
  }) {
    if (dir != null) {
      return MapEntry<String, SvgPicture>(
          key ?? name,
          SvgPicture.asset("$dirSvg$dir/$name.svg",
              semanticsLabel: "$name", color: color, height: size));
    } else {
      return MapEntry<String, SvgPicture>(
          key ?? name,
          SvgPicture.asset("$dirSvg$name.svg",
              semanticsLabel: "$name", color: color, height: size));
    }
  }

  static MapEntry<String, AssetImage> image(
    String name, {
    String? dir,
    String? key,
  }) {
    if (dir != null) {
      return MapEntry<String, AssetImage>(
          key ?? name, AssetImage("$dirImage$dir/$name.png"));
    } else {
      return MapEntry<String, AssetImage>(
          key ?? name, AssetImage("$dirImage$name.png"));
    }
  }

  MapEntry<String, SvgPicture> logo({Color? color}) => svg("logo", color: color);
  MapEntry<String, SvgPicture> smile({Color? color}) => svg("smile", color: color);

  //Icons
  MapEntry<String, SvgPicture> bndBgi({Color? color}) =>
      svg("bnd_bgi", color: color);
  MapEntry<String, SvgPicture> success({Color? color}) =>
      svg("success", color: color);
  MapEntry<String, SvgPicture> successOutlined({Color? color}) =>
      svg("success_outlined", color: color);
  MapEntry<String, SvgPicture> warning({Color? color}) =>
      svg("warning", color: color);
  MapEntry<String, SvgPicture> warningOutlined({Color? color}) =>
      svg("warning_outlined", color: color);
  MapEntry<String, SvgPicture> error({Color? color}) =>
      svg("error", color: color);
  MapEntry<String, SvgPicture> checkBoxChecked({Color? color}) =>
      svg("check_box_checked", color: color);
  MapEntry<String, SvgPicture> checkBoxUnchecked({Color? color}) =>
      svg("check_box_unchecked", color: color);
  MapEntry<String, SvgPicture> fingerPrint({Color? color}) =>
      svg("finger_print", color: color);
  MapEntry<String, SvgPicture> faceId({Color? color}) =>
      svg("face_id", color: color);
  MapEntry<String, SvgPicture> personOutline({Color? color}) =>
      svg("person_outline", color: color);
  MapEntry<String, SvgPicture> lockOutline({Color? color}) =>
      svg("lock_outline", color: color);
  MapEntry<String, SvgPicture> visibility({Color? color}) =>
      svg("visibility", color: color);
  MapEntry<String, SvgPicture> visibilityOff({Color? color}) =>
      svg("visibility_off", color: color);
  MapEntry<String, SvgPicture> biometric({Color? color}) =>
      svg("biometric", color: color);
  MapEntry<String, SvgPicture> remove({Color? color, double? size}) =>
      svg("remove", color: color, size: size);
  MapEntry<String, SvgPicture> checkCircleBlack({Color? color}) =>
      svg("check_circle_black", color: color);
  MapEntry<String, SvgPicture> checkedCircleBlack({Color? color}) =>
      svg("checked_circle_black", color: color);
  MapEntry<String, SvgPicture> envelopeOutlined({Color? color}) =>
      svg("envelope_outlined", color: color);
  MapEntry<String, SvgPicture> fb({Color? color}) => svg("fb", color: color);
  MapEntry<String, SvgPicture> google({Color? color}) =>
      svg("google", color: color);
  MapEntry<String, SvgPicture> phoneOutlined({Color? color}) =>
      svg("phone_outlined", color: color);

  MapEntry<String, SvgPicture> tabHomeSvg({Color? color}) =>
      svg("home", color: color, dir: "home");
  MapEntry<String, SvgPicture> tabSearchSvg({Color? color}) =>
      svg("search", color: color, dir: "home");
  MapEntry<String, SvgPicture> tabStoreSvg({Color? color}) =>
      svg("store", color: color, dir: "home");
  MapEntry<String, SvgPicture> tabSettingSvg({Color? color}) =>
      svg("user_circle", color: color, dir: "home");

  // Salon
  MapEntry<String, SvgPicture> bell({Color? color}) =>
      svg("bell", color: color, dir: "salon");
  MapEntry<String, SvgPicture> calendar({Color? color}) =>
      svg("calendar", color: color, dir: "salon");
  MapEntry<String, SvgPicture> chair({Color? color}) =>
      svg("chair", color: color, dir: "salon");
  MapEntry<String, SvgPicture> greenRound({Color? color}) =>
      svg("green_round", color: color, dir: "salon");
  MapEntry<String, SvgPicture> locationPoint({Color? color}) =>
      svg("location_point", color: color, dir: "salon");
  MapEntry<String, SvgPicture> star({Color? color}) =>
      svg("star", color: color, dir: "salon");
  MapEntry<String, SvgPicture> starLarge({Color? color}) =>
      svg("star_large", color: color, dir: "salon");
  MapEntry<String, SvgPicture> zzz({Color? color}) =>
      svg("zzz", color: color, dir: "salon");
  MapEntry<String, SvgPicture> clock({Color? color}) =>
      svg("clock", color: color, dir: "salon");

  // User
  MapEntry<String, SvgPicture> angleRight({Color? color}) =>
      svg("angle_right", color: color, dir: "user");
  MapEntry<String, SvgPicture> comment({Color? color}) =>
      svg("comment", color: color, dir: "user");
  MapEntry<String, SvgPicture> questionCircle({Color? color}) =>
      svg("question_circle", color: color, dir: "user");
  MapEntry<String, SvgPicture> userPlus({Color? color}) =>
      svg("user_plus", color: color, dir: "user");
  MapEntry<String, SvgPicture> xxx({Color? color}) =>
      svg("xxx", color: color, dir: "user");

  // Detail
  MapEntry<String, SvgPicture> checkBoxWarning({Color? color}) =>
      svg("check_box_warning", color: color, dir: "detail");
  MapEntry<String, SvgPicture> salonImg({Color? color}) =>
      svg("salon_image", color: color, dir: "detail");
  MapEntry<String, SvgPicture> service1({Color? color}) =>
      svg("service1", color: color, dir: "detail");
  MapEntry<String, SvgPicture> service2({Color? color}) =>
      svg("service2", color: color, dir: "detail");
  MapEntry<String, SvgPicture> service3({Color? color}) =>
      svg("service3", color: color, dir: "detail");
  MapEntry<String, SvgPicture> service4({Color? color}) =>
      svg("service4", color: color, dir: "detail");
  // MapEntry<String, SvgPicture> sendMessage({Color? color}) => svg("send_message", color: color, dir: "detail");

  // Tab bar icon
  String get tabHome => "${dirSvg}home/home.svg";
  String get tabSearch => "${dirSvg}home/search.svg";
  String get tabStore => "${dirSvg}home/store.svg";
  String get tabUser => "${dirSvg}home/user_circle.svg";
  String get salonImage => "${dirImage}salon/salon_image.png";
  String get sendMessage => "${dirImage}detail/send_message.png";
  String get salonDetail => "${dirImage}detail/salon_image.png";
}
