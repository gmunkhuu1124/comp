part of styles;

class _Color {
  const _Color();

  ///Opacity: [ 100% ]
  Color get background => const Color(0xFFFAFBFC);
  Color get disabled => const Color(0xFFEAEAEA);

  ///Opacity: [ 100% ]
  // Color get primary => const Color(0xFF0067FF);
  Color get primary => const Color(0xFF011F32);
  Color get second => const Color(0xFFF6B545);

  ///Opacity: [ 100%, 70%, 50% ]
  Color get dark => const Color(0xFF1B2330);
  Color get dark15 => const Color(0x141B2330);
  Color get dark70 => const Color(0xFF1B2330).withOpacity(.7);
  Color get dark50 => const Color(0xFF1B2330).withOpacity(.5);

  Color get white => const Color(0xFFFFFFFF);

  ///Opacity: [ 100%, 15% ]
  Color get success => const Color(0xFF00CF58);
  Color get success15 => const Color(0xFFF4BA46);

  ///Opacity: [ 100%, 15% ]
  Color get warning => const Color(0xFFF4BA46);
  Color get warning15 => const Color(0xFFF4BA46).withOpacity(.15);

  ///Opacity: [ 100%, 15% ]
  Color get error => const Color(0xFFD96448);
  Color get error15 => const Color(0xFFD96448).withOpacity(.15);

  ///Opacity: [ 100% ]
  Color get reminder => const Color(0xFFFFEABA);

  ///Divider color
  Color get divider => const Color(0xFF707070);

  Color get btnBg => const Color(0xFFF4F6F8);

  Color get txt => const Color(0xFF011F32).withOpacity(0.5);
  Color get btnTxt => const Color(0xFF011F32);
}
