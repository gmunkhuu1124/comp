part of styles;

class _FontSize {
  const _FontSize();

  /// FontSize = [24]
  final double xLarge = 24;

  /// FontSize = [17]
  final double large = 17;

  /// FontSize = [15]
  final double body = 15;

  /// FontSize = [13]
  final double small = 13;

  /// FontSize = [11]
  final double xSmall = 12;

  final double xxSmall = 11;
}
