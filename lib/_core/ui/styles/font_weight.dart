part of styles;

class _FontWeight {
  const _FontWeight();

  FontWeight get regular => FontWeight.w400;

  FontWeight get medium => FontWeight.w500;

  FontWeight get bold => FontWeight.bold;
}
