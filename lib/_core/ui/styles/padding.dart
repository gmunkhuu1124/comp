part of styles;

class _Padding {
  const _Padding();

  /// xxLarge = [36]
  double get xxxLarge => 36;

  /// xxLarge = [32]
  double get xxLarge => 32;

  /// xLarge = [24]
  double get xLarge => 24;

  /// large = [16]
  double get large => 16;

  /// medium = [12]
  double get medium => 12;

  /// small = [8]
  double get small => 8;

  /// xSmall = [4]
  double get xSmall => 4;

  double call() => medium;
}
