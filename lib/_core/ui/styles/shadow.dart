part of styles;

class _Shadow {
  const _Shadow();

  BoxShadow get light => BoxShadow(
        offset: Offset(0, 6),
        blurRadius: 30,
        color: Styles.color.dark15,
      );
}
