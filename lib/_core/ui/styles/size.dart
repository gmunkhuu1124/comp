part of styles;

class _Size {
  const _Size();

  //AppBar
  final double appBarHeight = 48;

  ///Button
  final double btnHeight = 57;
  final double btnBorderRadius = 12;
  final double btnBorderWidth = 1.5;

  ///TextField
  final double tbBorderRadius = 12;
  final double tbHeight = 48;

  ///IconButton
  final double iconHeight = 44;
  final double iconBorderRadius = 12;
  final double iconSize = 20;

  ///DropDownMenu
  final double cbHeight = 48;
  final double cbBorderRadius = 12;

  ///ListTile
  final double ltHeightMin = 52;
  final double ltHeight = 56;
  final double ltIconHeight = 48;

  ///BottomSheet
  final double bsHeaderHeight = 52;
  final double bsHeaderBorderRadius = 12;

  ///Dialog
  final double dialogIconSize = 72;
  final double dialogBorderRadius = 24;
  final double dialogMaxWidth = 311;

  ///SnackBar
  final double sbBorderRadius = 8;
  final double sbVerticalPadding = 10;
  final double sbHorizontalPadding = 16;
  final double sbIconSize = 24;
  final double sbHeight = 48;
  final int sbDuration = 3;

  ///Loader size
  final double loaderSize = 48;
}
