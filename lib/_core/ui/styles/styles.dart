library styles;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

part 'color.dart';
part 'font_size.dart';
part 'font_weight.dart';
part 'padding.dart';
part 'shadow.dart';
part 'size.dart';


class Styles {
  const Styles();
  static const _FontSize fontSize = const _FontSize();
  static const _Color color = const _Color();
  static const _Size size = const _Size();
  static const _FontWeight fontWeight = const _FontWeight();
  static const _Padding padding = const _Padding();
  static const _Shadow shadow = const _Shadow();

  static SystemUiOverlayStyle get systemUiOverlay => SystemUiOverlayStyle(
        // systemNavigationBarColor: Styles.color.primary,
        // systemNavigationBarIconBrightness: Brightness.light,
        statusBarColor: Styles.color.primary,
        statusBarIconBrightness: Brightness.light,
      );
}