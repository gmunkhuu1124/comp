part of widgets;


class CustomBottomSheet extends StatelessWidget {
  final Widget? child;
  final Widget? title;

  const CustomBottomSheet({
    Key? key,
    this.child,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final maxHeight = Styles.size.ltHeightMin * 5;
    final headerHeight = Styles.size.bsHeaderHeight;

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          height: headerHeight,
          child: Stack(
            fit: StackFit.expand,
            children: [
              Align(
                alignment: Alignment.center,
                child: DefaultTextStyle(
                  style: UIUtils.getTextStyleFromEnum(CustomTextStyle.large_medium)!,
                  child: title ?? Container(),
                ),
              ),
              Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: const EdgeInsets.all(4),
                    child: CustomIconButton(icon: Icon(Icons.cancel), onTap: () => Navigator.pop(context)),
                  )),
            ],
          ),
        ),
        Flexible(
          child: Container(
            height: maxHeight,
            child: child
            // NoGlow(
            //   child: child,
            // ),
          ),
        ),
      ],
    );
  }
}
