part of widgets;


class CustomPrimaryButton extends StatelessWidget {
  /// Стандарт хэмжээтэй Icon өгнө гэж бодож хийлээ
  final Widget? title;
  final Widget? icon;
  final Color? color;
  final Color? textColor;
  final double? maxWidth;
  final VoidCallback? onPressed;
  final double borderRadius;
  const CustomPrimaryButton({
    Key? key,
    this.icon,
    this.color,
    this.textColor,
    this.maxWidth,
    this.borderRadius = 12,
    required this.title,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final iconSize = Styles.size.btnHeight / 2;
    final btnHeigth = Styles.size.btnHeight;

    final paddingHorizontal = Styles.padding.large;
    final paddingVertical = Styles.padding();

    final childColor = onPressed != null ? textColor ?? Styles.color.white : Styles.color.dark;
    final bgColor = onPressed != null ? Styles.color.primary : Styles.color.disabled;

    return Material(
      borderRadius: BorderRadius.circular(borderRadius),
      color: color ?? bgColor,
      child: IconTheme(
        data: IconThemeData(color: childColor),
        child: DefaultTextStyle(
          style: UIUtils.getTextStyleFromEnum(CustomTextStyle.large_medium)!.copyWith(color: childColor),
          child: InkWell(
            onTap: onPressed,
            borderRadius: BorderRadius.circular(Styles.size.btnBorderRadius),
            child: ConstrainedBox(
              constraints: BoxConstraints(
                maxWidth: maxWidth ?? double.infinity,
                maxHeight: btnHeigth,
                minHeight: btnHeigth,
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(
                  vertical: paddingVertical,
                  horizontal: paddingHorizontal,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    icon != null
                        ? Container(
                            margin: title != null ? EdgeInsets.only(right: Styles.padding()) : null,
                            height: iconSize,
                            width: iconSize,
                            child: icon,
                          )
                        : Container(),
                    title ?? Container(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class CustomPlainButton extends StatelessWidget {
  /// Стандарт хэмжээтэй Icon өгнө гэж бодож хийлээ
  final Widget? icon;
  final Widget? title;
  final Color? color;
  final double? maxWidth;
  final Color? textColor;

  final VoidCallback? onPressed;

  const CustomPlainButton({
    Key? key,
    this.icon,
    this.color,
    this.maxWidth,
    this.textColor,
    required this.title,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final iconSize = Styles.size.btnHeight / 2;
    final btnHeigth = Styles.size.btnHeight;

    final paddingHorizontal = Styles.padding.large;
    final paddingVertical = Styles.padding();

    final childColor = onPressed != null ? textColor ?? Styles.color.primary : Styles.color.dark;
    final bgColor = onPressed != null ? Colors.transparent : Styles.color.background;

    return IconTheme(
      data: IconThemeData(color: childColor),
      child: DefaultTextStyle(
        style: UIUtils.getTextStyleFromEnum(CustomTextStyle.large_medium)!.copyWith(color: childColor),
        child: InkWell(
          onTap: onPressed,
          borderRadius: BorderRadius.circular(Styles.size.btnBorderRadius),
          child: ConstrainedBox(
            constraints: BoxConstraints(
              maxWidth: maxWidth ?? double.infinity,
            ),
            child: Container(
              padding: EdgeInsets.symmetric(
                vertical: paddingVertical,
                horizontal: paddingHorizontal,
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(Styles.size.btnBorderRadius),
                color: color ?? bgColor,
              ),
              height: btnHeigth,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  icon != null
                      ? Container(
                          margin: title != null ? EdgeInsets.only(right: Styles.padding()) : null,
                          height: iconSize,
                          width: iconSize,
                          child: icon,
                        )
                      : Container(),
                  title ?? Container(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class CustomIconButton extends StatelessWidget {
  final Widget? icon;
  final Color? backgroundColor;
  final Color? color;
  final Color? disabledColor;
  final VoidCallback? onTap;
  final double? size;

  const CustomIconButton({
    Key? key,
    required this.icon,
    this.color,
    this.disabledColor,
    this.backgroundColor,
    this.size,
    required this.onTap,
  }) : super(key: key);

  factory CustomIconButton.primary({
    Key? key,
    Widget? icon,
    VoidCallback? onTap,
  }) =>
      CustomIconButton(
        key: key,
        icon: icon,
        color: Colors.white,
        size: 48,
        backgroundColor: Styles.color.primary,
        onTap: onTap,
      );

  @override
  Widget build(BuildContext context) {
    final iconSize = Styles.size.iconHeight;
    final iconRadius = Styles.size.iconBorderRadius;
    final enabled = onTap != null;

    final theme = IconTheme.of(context);

    return UnconstrainedBox(
      child: Material(
        color: Colors.transparent,
        child: IconTheme(
          data: IconThemeData(color: enabled ? (color ?? theme.color) : disabledColor ?? Styles.color.background, size: 20),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(iconRadius),
            child: InkWell(
              onTap: onTap,
              onLongPress: onTap,
              onDoubleTap: onTap,
              borderRadius: BorderRadius.circular(iconRadius),
              child: Container(
                color: backgroundColor ?? Colors.transparent,
                height: size ?? iconSize,
                width: size ?? iconSize,
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: IntrinsicWidth(child: icon),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class CustomOutlinedButton extends StatelessWidget {
  /// Стандарт хэмжээтэй Icon өгнө гэж бодож хийлээ

  final Widget? icon;
  final Widget? title;
  final Color? color;
  final double? maxWidth;
  final Color? textColor;

  final VoidCallback? onPressed;

  const CustomOutlinedButton({
    Key? key,
    this.icon,
    this.color,
    this.textColor,
    this.maxWidth,
    required this.title,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final iconSize = Styles.size.btnHeight / 2;
    final btnHeigth = Styles.size.btnHeight - Styles.size.btnBorderWidth;
    final btnBorderWidth = Styles.size.btnBorderWidth;

    final paddingHorizontal = Styles.padding.large - btnBorderWidth;
    final paddingVertical = Styles.padding() - btnBorderWidth;

    final childColor = onPressed != null ? textColor ?? Styles.color.primary : Styles.color.dark;
    final borderColor = onPressed != null ? Styles.color.primary : Styles.color.background;
    final bgColor = onPressed != null ? Styles.color.white : Styles.color.background;

    return IconTheme(
      data: IconThemeData(color: childColor),
      child: DefaultTextStyle(
        style: UIUtils.getTextStyleFromEnum(CustomTextStyle.large_medium)!.copyWith(color: childColor),
        child: InkWell(
          onTap: onPressed,
          borderRadius: BorderRadius.circular(Styles.size.btnBorderRadius),
          child: ConstrainedBox(
            constraints: BoxConstraints(
              maxWidth: maxWidth ?? double.infinity,
            ),
            child: Container(
              padding: EdgeInsets.symmetric(
                vertical: paddingVertical,
                horizontal: paddingHorizontal,
              ),
              decoration: BoxDecoration(
                border: Border.all(
                  width: btnBorderWidth,
                  color: color ?? borderColor,
                ),
                borderRadius: BorderRadius.circular(Styles.size.btnBorderRadius),
                color: color ?? bgColor,
              ),
              height: btnHeigth,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  icon != null
                      ? Container(
                          margin: title != null ? EdgeInsets.only(right: Styles.padding()) : null,
                          height: iconSize,
                          width: iconSize,
                          child: icon,
                        )
                      : Container(),
                  title ?? Container(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
