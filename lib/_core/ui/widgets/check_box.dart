part of widgets;


class CustomFormCheckBox extends StatelessWidget {
  final bool? initialValue;
  final bool? enabled;
  final bool? required;
  final void Function(bool)? onChanged;
  final void Function(bool?)? onSaved;

  const CustomFormCheckBox({
    Key? key,
    this.initialValue,
    this.enabled,
    this.required,
    this.onChanged,
    this.onSaved,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final value = initialValue ?? false;
    final isEnabled = enabled ?? true;
    final isRequired = isEnabled ? (required ?? false) : false;
    return FormField<bool>(
      initialValue: value,
      onSaved: isEnabled ? onSaved : null,
      enabled: isEnabled,
      validator: (isRequired && isEnabled) ? Validator.mustBeChecked : Validator.none,
      builder: (FormFieldState<bool> field) => Column(
        children: [
          CustomCheckBox(
            enabled: isEnabled,
            value: field.value,
            onChanged: (val) {
              field.didChange(val);
              if (onChanged != null) onChanged!(val);
            },
          ),
        ],
      ),
    );
  }
}

class CustomCheckBox extends StatelessWidget {
  final bool? enabled;
  final bool? value;
  final void Function(bool)? onChanged;
  const CustomCheckBox({
    Key? key,
    this.enabled,
    this.value,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isEnabled = (enabled ?? true) && onChanged != null;

    final bgColor = (value ?? false)
        ? isEnabled
            ? Styles.color.primary
            : Styles.color.background
        : Colors.transparent;

    const size = 15.0;
    const borderWidth = 1.5;
    final border = Border.all(color: isEnabled ? Styles.color.primary : Styles.color.background, width: borderWidth);

    final icon = (value ?? false) ? Icon(Icons.check_rounded, color: Colors.white) : Container();

    return CustomIconButton(
      icon: Container(
          width: size,
          height: size,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            color: bgColor,
            border: border,
          ),
          child: OverflowBox(maxHeight: 44, maxWidth: 44, alignment: Alignment.center, child: icon)),
      onTap: isEnabled ? () => onChanged!(!(value ?? false)) : null,
    );
  }
}
