part of widgets;

class ConfirmWidget extends StatefulWidget {
  @override
  _ConfirmWidgetState createState() => _ConfirmWidgetState();
}

class _ConfirmWidgetState extends State<ConfirmWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 32),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AppPadding.xLarge,
            CustomText(
              "Баталгаажуулах".tr(),
              textAlign: TextAlign.left,
              newStyle:
                  TextStyle(fontSize: 26, fontWeight: Styles.fontWeight.bold),
              color: Styles.color.second,
            ),
            AppPadding.large,
            CustomText(
              "User****@mail.com хаяганд ирсэн\n4 оронтой кодыг оруулна уу.".tr(),
              style: CustomTextStyle.body_regular,
              color: Styles.color.txt,
            ),
            AppPadding.xxLarge,
            // CodeInput(
            //   length: 4,
            //   keyboardType: TextInputType.number,
            //   builder: CodeInputBuilders.containerized(
            //     totalSize: Size(45.0, 30.0),
            //     emptySize: Size(35.0, 30.0),
            //     filledSize: Size(35.0, 30.0),
            //     emptyDecoration: new BoxDecoration(
            //         border: Border(
            //             bottom: BorderSide(
            //       color: Styles.color.primary,
            //       width: 1,
            //     ))),
            //     emptyTextStyle: null,
            //     filledDecoration: new BoxDecoration(
            //       border: Border(
            //           bottom: BorderSide(
            //         color: Styles.color.primary,
            //         width: 1,
            //       )),
            //     ),
            //     filledTextStyle: TextStyle(
            //         color: Styles.color.primary,
            //         fontWeight: FontWeight.bold,
            //         fontSize: 24.0),
            //   ),
            //   onFilled: (value) {},
            //   onChanged: (value) {},
            // ),
            CustomPrimaryButton(
              onPressed: () {
              },
              title: "Үргэлжлүүлэх".toWidget(),
            ),
          ],
        ),
      ),
    );
  }
}