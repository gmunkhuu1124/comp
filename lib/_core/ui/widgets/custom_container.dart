part of widgets;

class CustomIconContainer extends StatelessWidget {
  final Widget icon;
  final double? padding;

  const CustomIconContainer({
    Key? key,
    required this.icon,
    this.padding,
  }) : super(key: key);

  factory CustomIconContainer.arrowForward() => CustomIconContainer(icon: Icon(Icons.arrow_forward_ios_rounded));

  @override
  Widget build(BuildContext context) {
    final iconSize = Styles.size.iconHeight;
    final iconRadius = Styles.size.iconBorderRadius;
    return IconTheme(
      data: IconThemeData(
        size: 13,
        color: Styles.color.dark,
      ),
      child: ClipRRect(
          borderRadius: BorderRadius.circular(iconRadius),
          child: SizedBox(
            height: iconSize,
            width: iconSize,
            child: Padding(
              padding: EdgeInsets.all(padding ?? 10),
              child: IntrinsicWidth(child: icon),
            ),
          )),
    );
  }
}
