part of widgets;

class CustomDropDownMenu<T> extends StatelessWidget {
  final String? hintText;
  final String? label;
  final bool? enabled;
  final AutovalidateMode? autovalidateMode;
  final List<CustomDropdownMenuItem<T>>? items;
  final T? initialValue;
  final void Function(T?)? onChange;
  final void Function(T?)? onSaved;

  final Widget? trailingIconWhenEmpty;
  final VoidCallback? onPressedWhenEmpty;
  final bool? isRequired;

  const CustomDropDownMenu({
    Key? key,
    this.hintText,
    this.label,
    this.enabled,
    this.onSaved,
    this.autovalidateMode,
    required this.items,
    this.trailingIconWhenEmpty,
    this.initialValue,
    this.isRequired,
    this.onPressedWhenEmpty,
    this.onChange,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // const paddingHorizontal = 16.0;
    // const paddingVertical = 15.0;

    final itemLength = items?.length ?? 0;
    final isEnabled = (enabled ?? true) ? itemLength > 0 : false;
    final validIntialValue = items?.firstWhereOrNull((element) => element.value == initialValue);
    final require = (isRequired ?? false);

    return FormField<T>(
        validator: require && isEnabled ? Validator.required : Validator.none,
        initialValue: validIntialValue?.value,
        autovalidateMode: autovalidateMode ?? AutovalidateMode.disabled,
        enabled: isEnabled,
        onSaved: isEnabled ? onSaved : null,
        builder: (FormFieldState<T> field) {
          final selectedItem = itemLength > 0 ? items?.firstWhereOrNull((element) => element.value == field.value) : null;

          if (initialValue == null && field.value == null && itemLength == 1 && require) {
            UIUtils.callback(() {
              field.didChange(items?[0].value);
            });
          }

          void _showPicker(BuildContext context) {
            UIUtils.hideKeyboard(context);
            UIUtils.showCustomBottomSheet(
                context: context,
                builder: (context) => CustomBottomSheet(
                      title: Text("Picker"),
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: itemLength,
                        itemBuilder: (context, index) => CustomListTile(
                          leading: items?[index].icon,
                          title: Text(items?[index].title ?? ""),
                          subtitle: items?[index].subtitle != null ? Text(items![index].subtitle!) : null,
                          onTap: () {
                            Navigator.pop(context);
                            field.didChange(items?[index].value);
                            if (onChange != null) onChange!(items![index].value);
                            if (items?[index].onTap != null) items![index].onTap!();
                          },
                        ),
                      ),
                    ));
          }

          return CustomInputDecorator(
            enabled: isEnabled,
            errorText: field.errorText,
            label: label,
            child: Material(
              color: Colors.transparent,
              child: CustomListTile(
                onTap: isEnabled
                    ? itemLength > 0
                        ? () {
                            UIUtils.hideKeyboard(context);
                            _showPicker(context);
                          }
                        : onPressedWhenEmpty
                    : null,
                leading: selectedItem?.icon,
                title: selectedItem?.title.toWidget() ?? hintText?.toWidget(),
                trailingIcon: Icon(Icons.keyboard_arrow_down_rounded),
              ),
              //   InkWell(
              //     borderRadius: BorderRadius.circular(Styles.size.tbBorderRadius),
              //     onTap: isEnabled
              //         ? itemLength > 0
              //             ? () {
              //                 UIUtils.hideKeyboard(context);
              //                 _showPicker(context);
              //               }
              //             : onPressedWhenEmpty
              //         : null,
              //     child: Row(
              //       children: [
              //         Expanded(
              //           child: Padding(
              //             padding: const EdgeInsets.symmetric(horizontal: paddingHorizontal, vertical: paddingVertical),
              //             child: title,
              //           ),
              //         ),
              //         Padding(
              //           padding: const EdgeInsets.all(2),
              //           child: CustomIconButton(
              //             icon: itemLength > 0 ? Icon(Icons.arrow_drop_down_rounded) : trailingIconWhenEmpty,
              //             onTap: null,
              //           ),
              //         ),
              //       ],
              //     ),
              //   ),
            ),
          );
        });
  }
}

class CustomDropdownMenuItem<T> {
  final T? value;
  final String title;
  final Widget? icon;
  final String? subtitle;
  final VoidCallback? onTap;
  CustomDropdownMenuItem({
    this.icon,
    required this.value,
    required this.title,
    this.subtitle,
    this.onTap,
  });

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CustomDropdownMenuItem<T> && other.value == value && other.title == title && other.icon == icon && other.subtitle == subtitle && other.onTap == onTap;
  }

  @override
  int get hashCode {
    return value.hashCode ^ title.hashCode ^ icon.hashCode ^ subtitle.hashCode ^ onTap.hashCode;
  }

  @override
  String toString() {
    return 'CustomDropdownMenuItem(value: $value, title: $title, icon: $icon, subtilte: $subtitle, onTap: $onTap)';
  }
}

// class DropdownButtonFormField<T> extends FormField<T> {
//   DropdownButtonFormField({
//     Key key,
//     @required this.items,
//     T initialValue,
//     String hintText,
//     // Widget disabledHint,
//     this.onChanged,
//     VoidCallback onTap,
//     int elevation = 8,
//     TextStyle style,
//     Widget icon,
//     FormFieldSetter<T> onSaved,
//     FormFieldValidator<T> validator,
//     AutovalidateMode autovalidateMode,
//   }) : super(
//           key: key,
//           onSaved: onSaved,
//           initialValue: initialValue,
//           validator: validator,
//           autovalidateMode: (autovalidateMode ?? AutovalidateMode.disabled),
//           builder: (FormFieldState<T> field) {
//             final _DropdownButtonFormFieldState<T> state = field as _DropdownButtonFormFieldState<T>;

//             const paddingHorizontal = 16.0;
//             const paddingVertical = 15.0;
//             final title = selectedItem == null
//                 ? CustomText(
//                     widget.hintText,
//                     style: CustomTextStyle.body_regular,
//                     color: Helper.color.dark50,
//                   )
//                 : CustomText(selectedItem.title, style: CustomTextStyle.body_medium);

//             return Focus(
//               canRequestFocus: false,
//               skipTraversal: true,
//               child: Builder(builder: (BuildContext context) {
//                 return InputDecorator(
//                   decoration: effectiveDecoration.copyWith(errorText: field.errorText),
//                   isEmpty: state.value == null,
//                   isFocused: Focus.of(context).hasFocus,
//                   child: DropdownButtonHideUnderline(
//                     child: DropdownButton<T>(
//                       items: items,
//                       selectedItemBuilder: selectedItemBuilder,
//                       value: state.value,
//                       hint: hint,
//                       disabledHint: disabledHint,
//                       onChanged: onChanged == null ? null : state.didChange,
//                       onTap: onTap,
//                       elevation: elevation,
//                       style: style,
//                       icon: icon,
//                       iconDisabledColor: iconDisabledColor,
//                       iconEnabledColor: iconEnabledColor,
//                       iconSize: iconSize,
//                       isDense: isDense,
//                       isExpanded: isExpanded,
//                       itemHeight: itemHeight,
//                       focusColor: focusColor,
//                       focusNode: focusNode,
//                       autofocus: autofocus,
//                       dropdownColor: dropdownColor,
//                     ),
//                   ),
//                 );
//               }),
//             );
//           },
//         );

//   final ValueChanged<T> onChanged;
//   final List<CustomDropdownMenuItem> items;

//   @override
//   FormFieldState<T> createState() => _DropdownButtonFormFieldState<T>();
// }

// class _DropdownButtonFormFieldState<T> extends FormFieldState<T> {
//   @override
//   DropdownButtonFormField<T> get widget => super.widget as DropdownButtonFormField<T>;
//   int get itemLength => widget.items?.length ?? 0;
//   CustomDropdownMenuItem<T> get initialValue => itemLength > 1 && widget.initialValue != null
//       ? widget.items.firstWhere((element) => element.value == widget.initialValue, orElse: null)
//       : null;

//   CustomDropdownMenuItem<T> get selectedItem => initialValue != null
//       ? initialValue
//       : selectedIndex != null && widget.items.length > 0
//           ? widget.items[selectedIndex]
//           : null;

//   @override
//   void didChange(T value) {
//     super.didChange(value);
//     assert(widget.onChanged != null);
//     widget.onChanged(value);
//   }

//   @override
//   void didUpdateWidget(DropdownButtonFormField<T> oldWidget) {
//     super.didUpdateWidget(oldWidget);
//     if (oldWidget.initialValue != widget.initialValue) {
//       setValue(widget.initialValue);
//     }
//   }
// }
