part of widgets;


class CustomInputDecorator extends StatelessWidget {
  final bool? enabled;
  final String? label;
  final Widget? child;
  final String? errorText;
  const CustomInputDecorator({
    Key? key,
    this.label,
    this.enabled,
    this.child,
    this.errorText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bool isValid = enabled! ? errorText == null : true;
    final bgColor = enabled!
        ? isValid
            ? Styles.color.btnBg
            : Styles.color.error15
        : Styles.color.disabled;
    // final bgColor = enabled!
    //     ? isValid
    //         ? Styles.color.white
    //         : Styles.color.error15
    //     : Styles.color.disabled;
    final label = this.label != null
        ? Padding(
            padding: EdgeInsets.fromLTRB(Styles.padding.small, 0, 0, Styles.padding.small),
            child: this.label!.toWidget(
                  style: CustomTextStyle.body_medium,
                  color: Styles.color.dark70,
                ))
        : Container();

    final errorMessage = Column(
      children: [
        AppPadding.xSmall,
        Container(
          height: 15,
          child: isValid
              ? null
              : CustomText(
                  errorText,
                  style: CustomTextStyle.small_medium,
                  color: Styles.color.error,
                ),
        ),
      ],
    );
    return Column(
      children: [
        Align(alignment: Alignment.centerLeft, child: label),
        ConstrainedBox(
          constraints: BoxConstraints(minHeight: Styles.size.tbHeight),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(Styles.size.tbBorderRadius),
              color: bgColor,
            ),
            child: child,
          ),
        ),
        Align(alignment: Alignment.centerRight, child: errorMessage),
      ],
    );
  }
}
