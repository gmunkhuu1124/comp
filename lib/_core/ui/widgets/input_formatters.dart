part of widgets;


/// [.] дарах үед курсорыг хамгийн ард байрлуулах
/// эсвэл тухайн [.] байгаа газраас хойш оруулсан лимитээр хязгээрлах
enum CurrencyFormatterMode {
  dotEnd,
  none,
}

class CurrencyInputFormatter extends TextInputFormatter {
  final int fractionLength;
  final CurrencyFormatterMode mode;
  final double? maxValue;

  CurrencyInputFormatter({
    this.fractionLength = 2,
    this.maxValue,
    this.mode = CurrencyFormatterMode.none,
  });

  //
  final String dot = ".";
  final String comma = "'";

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    String oldString = oldValue.text;
    int oldOffset = oldValue.selection.baseOffset;
    String newString = newValue.text;
    int newOffset = newValue.selection.baseOffset;

    if (_checkIsFirstChar(oldString, newString)) {
      if (newString.contains(dot) && newString.length == 1) {
        newString = "0.";
        newOffset = newString.length;
      } else {
        int value = int.tryParse(newString) ?? 0;
        if (value > 9) {
          value = value ~/ 10;
        }
        newString = value.toString();
      }
    } else {
      if (newString.contains(dot)) {
        newString = fractionLimiter(newString);
        String fractionStr = "";
        for (int i = 0; i < fractionLenth(newString); i++) {
          fractionStr += "0";
        }

        intl.NumberFormat formatter = intl.NumberFormat("#,###.$fractionStr", 'ts');

        double val = Parser.parseDouble(newString)!;
        if (maxValue != null && val > maxValue!) {
          newString = formatter.format(maxValue);
          newOffset = newString.length;
        } else {
          newString = formatter.format(val);

          if (newString.indexOf(dot) == 0) {
            newString = "0$newString";
          }

          if (newOffset < oldOffset) {
            oldOffset = newOffset;
          }
        }
      } else {
        if (newString.isNotEmpty) {
          intl.NumberFormat formatter = intl.NumberFormat("#,###", 'ts');
          if (dot.allMatches(oldString).length > 0) {
            newString = oldString.substring(0, oldString.indexOf(dot));
          }

          int val = Parser.parseInt(newString)!;
          if (maxValue != null && val > maxValue!) {
            newString = formatter.format(maxValue);
            newOffset = newString.length;
          } else {
            newString = formatter.format(val);
          }
        }
      }
    }
    int shiftAmount = calculateCursorOffset(oldString, newString);
    try {
      newOffset = newOffset + shiftAmount;
      if (shiftAmount == 0 && oldString == newString && oldOffset > newOffset) newOffset = oldOffset;
      newOffset = offsetLimiter(newOffset, newString);
    } catch (e) {
      print(e);
    }
    return TextEditingValue(text: newString, selection: TextSelection.fromPosition(TextPosition(offset: newOffset)));
  }

  int fractionLenth(String newText) {
    return newText.split(dot)[1].length;
  }

  bool isFirstDot(String oldText, String newText) {
    return !oldText.contains(dot) && newText.contains(dot);
  }

  bool isInValidDot(String text) {
    int dotLength = dot.allMatches(text).length;
    return dotLength > 1;
  }

  String fractionLocator(String text) {
    final cleanText = text.replaceAll(dot, "");
    return "$cleanText$dot";
  }

  String fractionLimiter(String text) {
    int dotIndex = text.indexOf(dot);
    int endIndex = (dotIndex + 1) + fractionLength;
    if (dotIndex == -1) return text;
    if (endIndex > text.length) endIndex = text.length;
    // if (text.length < endIndex) {}

    var arr = text.split(dot);
    String fraction = "";
    for (int i = 1; i < arr.length; i++) {
      fraction += arr[i];
    }
    text = "${arr[0]}$dot$fraction";
    try {
      return text.substring(0, endIndex);
    } catch (e) {
      return text;
    }
  }

  int calculateCursorOffset(String oldValue, String newValue) {
    return comma.allMatches(newValue).length - comma.allMatches(oldValue).length;
  }

  int offsetLimiter(int offset, String input) {
    if (offset > input.length) {
      return input.length;
    } else {
      return offset;
    }
  }

  bool _checkIsFirstChar(String oldValue, String newValue) {
    if (newValue.length > 1) return false;

    double oldVal = Parser.parseDouble(oldValue)!;

    return oldVal == 0 && newValue.isNotEmpty;
  }
}
