part of widgets;

class CustomLabel extends StatelessWidget {
  final String label;

  const CustomLabel(this.label, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: Styles.padding.small, bottom: Styles.padding.small),
      child: label.toWidget(style: CustomTextStyle.body_medium, color: Styles.color.dark70),
    );
  }
}
