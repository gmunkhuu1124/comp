part of widgets;


class CustomListTile extends StatelessWidget {
  final Widget? leading;
  //Trailing
  final Widget? trailingIcon;
  final Widget? trailingTitle;
  final Widget? trailingSubtitle;

  final Widget? title;
  final Widget? subtitle;

  final int? titleMaxLines;
  final bool titleMultiLine;
  final int? subtitleMaxLines;
  final bool subtitleMultiLine;
  final bool divider;

  final VoidCallback? onTap;

  const CustomListTile({
    Key? key,
    this.leading,
    this.onTap,
    this.trailingIcon,
    this.title,
    this.titleMaxLines,
    this.subtitle,
    this.trailingTitle,
    this.trailingSubtitle,
    this.titleMultiLine = false,
    this.subtitleMaxLines,
    this.divider = false,
    this.subtitleMultiLine = false,
  }) : super(key: key);

  bool get hasSubtitle => subtitle != null;
  bool get hasTrailingSubtitle => trailingSubtitle != null;
  bool get hasTitle => title != null;
  bool get hasTrailingTitle => trailingTitle != null;

  @override
  Widget build(BuildContext context) {
    if (hasTrailingSubtitle) assert(hasTrailingTitle && hasTrailingSubtitle);

    final double minHeight = Styles.size.ltHeightMin;
    final double leadingMaxHeight = Styles.size.ltIconHeight;

    final titleStyle = UIUtils.getTextStyleFromEnum(CustomTextStyle.large_regular)!;
    final subtitleStyle = UIUtils.getTextStyleFromEnum(CustomTextStyle.small_regular, Styles.color.dark50);
    final trailingTitleStyle = UIUtils.getTextStyleFromEnum(CustomTextStyle.body_regular);
    final trailingSubtitleStyle = UIUtils.getTextStyleFromEnum(CustomTextStyle.small_regular, Styles.color.dark50);

    final titleVerticalPadding = hasSubtitle
        ? 8.0
        : hasTrailingSubtitle
            ? 18.0
            : 16.0;
    final paddingVertical = hasSubtitle ? 6.0 : 4.0;
    final borderWidth = .5;

    final hasTrailingText = hasTrailingTitle || hasTrailingSubtitle;

    final tralingTextVerticalPadding = hasTrailingSubtitle ? 8.0 : 18.0;
    final tralingTextHorizontalPadding = 12.0;
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: onTap,
        child: ConstrainedBox(
            constraints: BoxConstraints(minHeight: minHeight),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                leading != null
                    ? Padding(
                        padding: EdgeInsets.symmetric(horizontal: 12, vertical: paddingVertical),
                        child: ConstrainedBox(constraints: BoxConstraints(maxHeight: leadingMaxHeight, maxWidth: leadingMaxHeight), child: Center(child: leading)),
                      )
                    : AppPadding.large,
                Expanded(
                  child: Stack(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.symmetric(vertical: titleVerticalPadding),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    DefaultTextStyle(
                                      style: titleStyle,
                                      child: title ?? Container(),
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.start,
                                      maxLines: titleMultiLine ? (titleMaxLines ?? 3) : 1,
                                    ),
                                    subtitle != null
                                        ? Column(
                                            children: [
                                              SizedBox(
                                                height: 5,
                                              ),
                                              DefaultTextStyle(
                                                style: subtitleStyle!,
                                                child: subtitle!,
                                                textAlign: TextAlign.start,
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: subtitleMultiLine ? (subtitleMaxLines ?? 3) : 1,
                                              ),
                                            ],
                                          )
                                        : Container()
                                  ],
                                ),
                              ),
                            ),
                          ),
                          if (hasTrailingText)
                            Padding(
                              padding: EdgeInsets.symmetric(
                                vertical: tralingTextVerticalPadding,
                                horizontal: tralingTextHorizontalPadding,
                              ),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    DefaultTextStyle(
                                      style: trailingTitleStyle!,
                                      child: trailingTitle!,
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.start,
                                      maxLines: 1,
                                    ),
                                    trailingSubtitle != null
                                        ? Column(
                                            children: [
                                              SizedBox(
                                                height: 7,
                                              ),
                                              DefaultTextStyle(
                                                style: trailingSubtitleStyle!,
                                                child: trailingSubtitle!,
                                                textAlign: TextAlign.start,
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 1,
                                              ),
                                            ],
                                          )
                                        : Container()
                                  ],
                                ),
                              ),
                            ),
                          trailingIcon != null
                              ? Padding(
                                  padding: EdgeInsets.fromLTRB(hasTrailingText ? 0 : 12, paddingVertical, paddingVertical, paddingVertical),
                                  child: ConstrainedBox(
                                    constraints: BoxConstraints(maxHeight: Styles.size.iconHeight, maxWidth: Styles.size.iconHeight),
                                    child: Center(child: trailingIcon),
                                  ),
                                )
                              : AppPadding.large,
                        ],
                      ),
                      if (divider)
                        Positioned(
                          bottom: 0,
                          left: 0,
                          right: 12,
                          child: Container(
                            width: double.infinity,
                            margin: const EdgeInsets.only(right: 12),
                            decoration: BoxDecoration(border: Border(bottom: BorderSide(width: borderWidth, color: Styles.color.dark.withOpacity(.15)))),
                          ),
                        )
                    ],
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
