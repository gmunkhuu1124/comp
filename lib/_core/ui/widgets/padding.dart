part of widgets;

class AppPadding extends StatelessWidget {
  final double padding;

  const AppPadding({Key? key, required this.padding}) : super(key: key);

  /// xxLarge = [36]
  static AppPadding get xxxLarge => AppPadding(padding: Styles.padding.xxxLarge);

  /// xxLarge = [32]
  static AppPadding get xxLarge => AppPadding(padding: Styles.padding.xxLarge);

  /// xLarge = [24]
  static AppPadding get xLarge => AppPadding(padding: Styles.padding.xLarge);

  /// large = [16]
  static AppPadding get large => AppPadding(padding: Styles.padding.large);

  /// medium = [12]
  static AppPadding get medium => AppPadding(padding: Styles.padding.medium);

  /// small = [8]
  static AppPadding get small => AppPadding(padding: Styles.padding.small);

  /// xSmall = [4]
  static AppPadding get xSmall => AppPadding(padding: Styles.padding.xSmall);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: padding,
      height: padding,
    );
  }
}
