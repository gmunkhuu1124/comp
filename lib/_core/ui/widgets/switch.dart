part of widgets;


// class CustomSwitch extends StatelessWidget {
//   final bool? initialValue;
//   final void Function(bool)? onChanged;
//   final void Function(bool?)? onSaved;
//   final bool? enabled;

//   const CustomSwitch({
//     Key? key,
//     this.initialValue,
//     this.enabled,
//     this.onChanged,
//     this.onSaved,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     final activeColor = Styles.color.primary;
//     final inActiveColor = Styles.color.dark50;
//     final value = initialValue ?? false;
//     final isEnabled = enabled ?? true;
//     return FormField<bool>(
//       initialValue: value,
//       onSaved: isEnabled ? onSaved : null,
//       enabled: isEnabled,
//       builder: (FormFieldState<bool> field) {
//         return FlutterSwitch(
//           // disabled: !isEnabled,
//           width: 40,
//           height: 24,
//           toggleSize: 20.0,
//           value: field.value!,
//           borderRadius: 24.0,
//           padding: 2.0,
//           activeColor: activeColor,
//           inactiveColor: inActiveColor,
//           onToggle: (val) {
//             field.didChange(val);
//             if (onChanged != null) onChanged!(val);
//           },
//         );
//       },
//     );
//   }
// }
