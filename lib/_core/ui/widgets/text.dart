part of widgets;



class CustomText extends StatelessWidget {
  final String? gender;
  final String? data;
  final Color? color;
  final Map<String, String>? namedArgs;
  final List<String>? args;
  final InlineSpan? textSpan;
  final CustomTextStyle? style;
  final TextStyle? newStyle;
  final StrutStyle? strutStyle;
  final TextAlign? textAlign;
  final TextDirection? textDirection;
  final Locale? locale;
  final bool? softWrap;
  final TextOverflow? overflow;
  final double? textScaleFactor;
  final int? maxLines;
  final String? semanticsLabel;
  final TextWidthBasis? textWidthBasis;
  final ui.TextHeightBehavior? textHeightBehavior;

  const CustomText(
    this.data, {
    Key? key,
    this.style,
    this.args,
    this.color,
    this.strutStyle,
    this.namedArgs,
    this.textAlign,
    this.gender,
    this.newStyle,
    this.textDirection,
    this.locale,
    this.softWrap,
    this.overflow,
    this.textScaleFactor,
    this.maxLines,
    this.semanticsLabel,
    this.textWidthBasis,
    this.textHeightBehavior,
  })  : textSpan = null,
        super(key: key);
  @override
  Widget build(BuildContext context) {
    return Text(
      lc.tr(data ?? "", args: args, namedArgs: namedArgs, gender: gender),
      style: newStyle ?? UIUtils.getTextStyleFromEnum(style, color),
      strutStyle: strutStyle,
      textAlign: textAlign,
      textDirection: textDirection,
      locale: locale,
      softWrap: softWrap,
      overflow: overflow,
      textScaleFactor: textScaleFactor,
      maxLines: maxLines,
      semanticsLabel: semanticsLabel,
      textWidthBasis: textWidthBasis,
      textHeightBehavior: textHeightBehavior,
    );
  }
}
