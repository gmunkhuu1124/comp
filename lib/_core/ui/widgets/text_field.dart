part of widgets;

typedef FutureOrCallBack = FutureOr<void> Function();

class CustomTextFormField extends FormField<String> {
  /// Creates a [FormField] that contains a [TextField].
  ///
  /// When a [controller] is specified, [initialValue] must be null (the
  /// default). If [controller] is null, then a [TextEditingController]
  /// will be constructed automatically and its `text` will be initialized
  /// to [initialValue] or the empty string.
  ///
  /// For documentation about the various parameters, see the [TextField] class
  /// and [new TextField], the constructor.
  CustomTextFormField({
    Key? key,
    this.controller,
    this.focusNode,
    String? label,
    String? hintText,
    String? errorText,
    String? initialValue,
    Widget? suffixIcon,
    Widget? prefixIcon,
    TextInputType? keyboardType,
    TextCapitalization textCapitalization = TextCapitalization.none,
    TextInputAction? textInputAction,
    TextStyle? style,
    StrutStyle? strutStyle,
    TextDirection? textDirection,
    TextAlign textAlign = TextAlign.start,
    TextAlignVertical? textAlignVertical,
    bool autofocus = false,
    bool readOnly = false,
    bool showCancelButton = false,
    ToolbarOptions? toolbarOptions,
    bool? showCursor,
    String obscuringCharacter = '•',
    bool obscureText = false,
    bool autocorrect = true,
    SmartDashesType? smartDashesType,
    SmartQuotesType? smartQuotesType,
    bool enableSuggestions = true,
    // MaxLengthEnforcement maxLengthEnforcement,
    int maxLines = 1,
    int? minLines,
    bool expands = false,
    int? maxLength,
    ValueChanged<String>? onChanged,
    GestureTapCallback? onTap,
    VoidCallback? onEditingComplete,
    ValueChanged<String>? onFieldSubmitted,
    FormFieldSetter<String>? onSaved,
    FormFieldValidator<String>? validator,
    // List<TextInputFormatter> inputFormatters,
    bool? enabled,
    double cursorWidth = 2.0,
    double? cursorHeight,
    List<TextInputFormatter>? inputFormatters,
    Radius? cursorRadius,
    Color? cursorColor,
    Brightness? keyboardAppearance,
    EdgeInsets scrollPadding = const EdgeInsets.all(20.0),
    bool enableInteractiveSelection = true,
    TextSelectionControls? selectionControls,
    InputCounterWidgetBuilder? buildCounter,
    ScrollPhysics? scrollPhysics,
    Iterable<String>? autofillHints,
    AutovalidateMode? autovalidateMode,
  })  : assert(initialValue == null || controller == null),
        assert(obscuringCharacter.length == 1),
        // assert(
        //   maxLengthEnforcement == null,
        //   'maxLengthEnforced is deprecated, use only maxLengthEnforcement',
        // ),
        assert(maxLines > 0),
        assert(minLines == null || minLines > 0),
        assert(
          (minLines == null) || (maxLines >= minLines),
          "minLines can't be greater than maxLines",
        ),
        assert(
          !expands || (minLines == null),
          'minLines and maxLines must be null when expands is true.',
        ),
        assert(!obscureText || maxLines == 1,
            'Obscured fields cannot be multiline.'),
        assert(maxLength == null || maxLength > 0),
        super(
          key: key,
          initialValue:
              controller != null ? controller.text : (initialValue ?? ''),
          onSaved: (enabled ?? true) ? onSaved : null,
          validator: (enabled ?? true) ? validator : (_) => null,
          enabled: enabled ?? true,
          autovalidateMode: (autovalidateMode ?? AutovalidateMode.disabled),
          builder: (FormFieldState<String> field) {
            final _CustomTextFormFieldState state =
                field as _CustomTextFormFieldState;
            final verticalPadding = 20.0;
            final prefixPadding =
                prefixIcon != null ? 2.0 : Styles.padding.large;
            final suffixPadding =
                suffixIcon != null ? 2.0 : Styles.padding.large;
            final hintStyle =
                UIUtils.getTextStyleFromEnum(CustomTextStyle.body_regular)!
                    .copyWith(color: Styles.color.dark50);

            Widget? _iconBuilder(Widget? icon) {
              // final iconColor = (state._effectivefocusNode?.hasFocus ?? false) ? Styles.color.dark : Styles.color.dark50;
              final iconColor = (state._effectivefocusNode?.hasFocus ?? false)
                  ? Styles.color.primary
                  : Styles.color.primary;
              return icon != null
                  ? IconTheme(
                      data: IconThemeData(color: iconColor),
                      child: Padding(
                          padding: const EdgeInsets.all(2.0), child: icon))
                  : null;
            }

            final InputDecoration effectiveDecoration = (InputDecoration(
              contentPadding: EdgeInsets.fromLTRB(prefixPadding,
                  verticalPadding, suffixPadding, verticalPadding),
              hintStyle: hintStyle,
              hintText: lc.tr(hintText ?? ""),
              enabled: enabled ?? true,
              isCollapsed: true,
              errorStyle: TextStyle(fontSize: 0, height: 0),
              errorBorder: InputBorder.none,
              border: InputBorder.none,
              counterText: "",
              suffixIcon: _iconBuilder(
                suffixIcon != null
                    ? suffixIcon
                    : showCancelButton
                        ? state._effectivefocusNode!.hasFocus
                            ? CustomIconButton(
                                icon: Icon(Icons.cancel_rounded),
                                color: Styles.color.dark,
                                onTap: state.clear,
                              )
                            : null
                        : null,
              ),
              prefixIcon: _iconBuilder(prefixIcon),
            )).applyDefaults(Theme.of(field.context).inputDecorationTheme);
            void onChangedHandler(String value) {
              field.didChange(value);
              if (onChanged != null) {
                onChanged(value);
              }
            }

            return CustomInputDecorator(
              errorText: errorText ?? field.errorText,
              enabled: enabled ?? true,
              label: label,
              child: TextField(
                controller: state._effectiveController,
                focusNode: state._effectivefocusNode,
                inputFormatters: inputFormatters,
                decoration:
                    effectiveDecoration.copyWith(errorText: field.errorText),
                keyboardType: keyboardType,
                textInputAction: textInputAction,
                style: style ??
                    UIUtils.getTextStyleFromEnum(
                        CustomTextStyle.body_medium, Styles.color.btnTxt),
                strutStyle: strutStyle,
                textAlign: textAlign,
                textAlignVertical: textAlignVertical,
                textDirection: textDirection,
                textCapitalization: textCapitalization,
                autofocus: autofocus,
                toolbarOptions: toolbarOptions,
                readOnly: readOnly,
                showCursor: showCursor,
                obscuringCharacter: obscuringCharacter,
                obscureText: obscureText,
                autocorrect: autocorrect,
                smartDashesType: smartDashesType ??
                    (obscureText
                        ? SmartDashesType.disabled
                        : SmartDashesType.enabled),
                smartQuotesType: smartQuotesType ??
                    (obscureText
                        ? SmartQuotesType.disabled
                        : SmartQuotesType.enabled),
                enableSuggestions: enableSuggestions,
                // maxLengthEnforcement: maxLengthEnforcement,
                maxLines: maxLines,
                minLines: minLines,
                expands: expands,
                maxLength: maxLength,
                onChanged: onChangedHandler,
                onTap: onTap,
                onEditingComplete: onEditingComplete,
                onSubmitted: onFieldSubmitted,
                // inputFormatters: inputFormatters,
                enabled: enabled ?? true,
                cursorWidth: cursorWidth,
                cursorHeight: cursorHeight,
                cursorRadius: cursorRadius,
                cursorColor: cursorColor,
                scrollPadding: scrollPadding,
                scrollPhysics: scrollPhysics,
                keyboardAppearance: keyboardAppearance,
                enableInteractiveSelection: enableInteractiveSelection,
                // selectionControls: selectionControls,
                buildCounter: buildCounter,
                autofillHints: autofillHints,
              ),
            );
          },
        );

  final TextEditingController? controller;
  final FocusNode? focusNode;

  @override
  _CustomTextFormFieldState createState() => _CustomTextFormFieldState();
}

class _CustomTextFormFieldState extends FormFieldState<String> {
  TextEditingController? _controller;
  FocusNode? _focusNode;

  TextEditingController? get _effectiveController =>
      widget.controller ?? _controller;
  FocusNode? get _effectivefocusNode => widget.focusNode ?? _focusNode;

  @override
  CustomTextFormField get widget => super.widget as CustomTextFormField;

  @override
  void initState() {
    super.initState();
    if (widget.controller == null) {
      _controller = TextEditingController(text: widget.initialValue);
    } else {
      widget.controller!.addListener(_handleControllerChanged);
    }
    if (widget.focusNode == null) {
      _focusNode = FocusNode();
    }
    _effectivefocusNode!.addListener(() {
      didChange(_effectiveController!.text);
    });
  }

  @override
  void didUpdateWidget(CustomTextFormField oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.controller != oldWidget.controller) {
      oldWidget.controller?.removeListener(_handleControllerChanged);
      widget.controller?.addListener(_handleControllerChanged);

      if (oldWidget.controller != null && widget.controller == null)
        _controller =
            TextEditingController.fromValue(oldWidget.controller!.value);
      if (widget.controller != null) {
        setValue(widget.controller!.text);
        if (oldWidget.controller == null) _controller = null;
      }
    }
  }

  @override
  void dispose() {
    widget.controller?.removeListener(_handleControllerChanged);
    _effectivefocusNode!.removeListener(() {
      didChange(_effectiveController!.text);
    });
    super.dispose();
  }

  @override
  void didChange(String? value) {
    super.didChange(value);

    if (_effectiveController!.text != value)
      _effectiveController!.text = value ?? '';
  }

  @override
  void reset() {
    _effectiveController!.text = widget.initialValue ?? '';
    _effectivefocusNode!.unfocus();
    super.reset();
  }

  void _handleControllerChanged() {
    if (_effectiveController!.text != value)
      didChange(_effectiveController!.text);
  }

  void clear() {
    // _effectivefocusNode?.unfocus();
    // _effectivefocusNode?.canRequestFocus = false;
    _effectiveController!.text = "";
    didChange(_effectiveController!.text);
    // _effectivefocusNode?.canRequestFocus = true;
  }
}

class CustomRegTextField extends StatefulWidget {
  final TextEditingController controller;
  final FocusNode? focusNode;
  final String label;
  final String hintText;
  final void Function(String)? onChanged;
  final void Function(String)? onSubmitted;

  CustomRegTextField({
    Key? key,
    required this.label,
    required this.hintText,
    required this.controller,
    this.onSubmitted,
    this.focusNode,
    this.onChanged,
  }) : super(key: key);

  @override
  CustomRegTextFieldState createState() => CustomRegTextFieldState();
}

class CustomRegTextFieldState extends State<CustomRegTextField> {
  late TextEditingController controller;
  late TextEditingController regNumberController;

  FocusNode? focusNode;

  void requestFocus() {
    _selectFirstChar();
  }

  Future<void> _selectFirstChar() async {
    UIUtils.hideKeyboard(context);
    UIUtils.showRegSelectorDialog(context, (char) {
      updateController(
        char,
        RegNoUtils.getSecondLetter(controller.text),
        regNumberController.text,
      );
    });
  }

  Future<void> _selectSecondChar() async {
    UIUtils.hideKeyboard(context);
    UIUtils.showRegSelectorDialog(context, (char) {
      updateController(
        RegNoUtils.getFirstLetter(controller.text),
        char,
        regNumberController.text,
      );
    });
  }

  @override
  void initState() {
    controller = widget.controller;
    regNumberController =
        TextEditingController(text: RegNoUtils.getNumber(controller.text));
    focusNode = widget.focusNode ?? FocusNode();
    regNumberController.addListener(() {
      updateController(
        RegNoUtils.getFirstLetter(controller.text),
        RegNoUtils.getSecondLetter(controller.text),
        regNumberController.text,
      );
    });
    controller.addListener(() {
      if (mounted) setState(() {});
    });
    super.initState();
  }

  @override
  dispose() {
    controller.removeListener(() {});
    regNumberController.removeListener(() {});
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Text(
        Padding(
          padding: EdgeInsets.fromLTRB(
              Styles.padding.small, 0, 0, Styles.padding.small),
          child: widget.label.toWidget(
            style: CustomTextStyle.body_medium,
            color: Styles.color.dark70,
          ),
        ),
        //   textAlign: TextAlign.left,
        //   style: TextStyle(
        //     fontSize: 14,
        //     fontWeight: FontWeight.w400,
        //     color: Color(0xFF8D98AB),
        //   ),
        // ),
        SizedBox(height: 10),
        Container(
          height: 48,
          child: Row(
            children: <Widget>[
              _buildRegSelector(
                title: RegNoUtils.getFirstLetter(controller.text),
                onPress: _selectFirstChar,
              ),
              SizedBox(width: 10.0),
              _buildRegSelector(
                title: RegNoUtils.getSecondLetter(controller.text),
                onPress: _selectSecondChar,
              ),
              SizedBox(width: 10.0),
              Expanded(
                child: Container(
                  height: 48,
                  width: double.infinity,
                  alignment: Alignment.center,
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  decoration: BoxDecoration(
                    color: Styles.color.btnBg,
                    borderRadius: BorderRadius.circular(10),
                    // border: Border.all(width: 1),
                    boxShadow: [
                      // BoxShadow(
                      //   color: Colors.grey.withOpacity(0.5),
                      //   spreadRadius: 5,
                      //   blurRadius: 7,
                      //   offset: Offset(0, 3), // changes position of shadow
                      // ),
                    ],
                  ),
                  child: TextField(
                    controller: regNumberController,
                    onChanged: widget.onChanged,
                    onSubmitted: widget.onSubmitted,
                    focusNode: focusNode,
                    maxLength: 8,
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                    ],
                    keyboardType: TextInputType.number,
                    style: TextStyle(
                      fontSize: 14,
                      color: Color(0xFF1B2330),
                      fontWeight: FontWeight.bold,
                    ),
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.symmetric(
                        vertical: 15,
                      ),
                      isCollapsed: true,
                      hintText: widget.hintText,
                      border: InputBorder.none,
                      hintStyle: TextStyle(
                        fontSize: 14,
                        color: Color(0xFF1B2330),
                        fontWeight: FontWeight.w300,
                      ),
                      counterText: "",
                      disabledBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      focusedErrorBorder: InputBorder.none,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  void updateController(String firstChar, String secondChar, String number) {
    controller.text = RegNoUtils.consolidate(
      firstChar,
      secondChar,
      number,
    );
    if (widget.onChanged != null) widget.onChanged!(controller.text);
  }

  GestureDetector _buildRegSelector({
    required String title,
    required GestureTapCallback? onPress,
  }) {
    bool isEmpty = title.trim().isEmpty;
    return GestureDetector(
      onTap: onPress,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Styles.color.btnBg,
          // border: Border.all(width: 1),
          boxShadow: [
            // BoxShadow(
            //   color: Colors.grey.withOpacity(0.5),
            //   spreadRadius: 5,
            //   blurRadius: 7,
            //   offset: Offset(0, 3), // changes position of shadow
            // ),
          ],
        ),
        height: 48,
        width: 48,
        child: Center(
            child: Text(
          isEmpty ? "А" : title,
          style: TextStyle(
            fontSize: 14,
            color: isEmpty ? Color(0xFF8D98AB) : Color(0xFF1B2330),
            fontWeight: isEmpty ? FontWeight.w300 : FontWeight.bold,
          ),
        )),
      ),
    );
  }
}

class RegNoUtils {
  static String getFirstLetter(String text) {
    if (text == null || text.isEmpty) {
      return " ";
    }

    final letter = text.substring(0, 1);
    return _isNumber(letter) ? "" : letter;
  }

  static String getSecondLetter(String text) {
    if (text == null || text.isEmpty || text.length < 2) {
      return " ";
    }

    final letter = text.substring(1, 2);

    return _isNumber(letter) ? "" : letter;
  }

  static String getNumber(String text) {
    if (text == null || text.isEmpty || text.length < 3) {
      return "";
    }

    final letter = text.substring(2);

    return _isNumber(letter) ? letter : "";
  }

  static bool _isNumber(String text) {
    return int.tryParse(text) != null;
  }

  static String consolidate(
      String firstChar, String secondChar, String number) {
    return firstChar + secondChar + number;
  }
}
