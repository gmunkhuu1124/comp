library widgets;

import 'dart:async';
// import 'dart:io';
import 'dart:ui' as ui show TextHeightBehavior;
import 'dart:ui';

import 'package:flutter/services.dart';

import '../../core.dart';

import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart' as lc;
import 'package:flutter/foundation.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart' as intl;
import 'package:collection/collection.dart';

part 'text.dart';
part 'label.dart';
part 'padding.dart';
part 'shaker.dart';
part 'check_box.dart';
part 'button.dart';
part 'drop_down_menu.dart';
part 'bottom_sheet.dart';
part 'list_tile.dart';
part 'input_decorator.dart';
part 'input_formatters.dart';
part 'text_field.dart';
part 'app_bar.dart';
part 'custom_container.dart';
part 'confirm.dart';