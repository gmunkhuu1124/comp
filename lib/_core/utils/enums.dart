part of utils;


enum CustomTextStyle {
  /// FontSize = [24],
  /// FontWeight = w800
  xLarge_bold,

  /// FontSize = [24],
  /// FontWeight = w500
  xLarge_medium,

  /// FontSize = [24],
  /// FontWeight = w400
  xLarge_regular,

  /// FontSize = [17],
  /// FontWeight = w800
  large_bold,

  /// FontSize = [17],
  /// FontWeight = w500
  large_medium,

  /// FontSize = [17],
  /// FontWeight = w400
  large_regular,

  /// FontSize = [15],
  /// FontWeight = w800
  body_bold,

  /// FontSize = [15],
  /// FontWeight = w500
  body_medium,

  /// FontSize = [15],
  /// FontWeight = w400
  body_regular,

  /// FontSize = [13],
  /// FontWeight = w800
  small_bold,

  /// FontSize = [13],
  /// FontWeight = w500
  small_medium,

  /// FontSize = [13],
  /// FontWeight = w400
  small_regular,

  /// FontSize = [12],
  /// FontWeight = w800
  xSmall_bold,

  /// FontSize = [12],
  /// FontWeight = w500
  xSmall_medium,

  /// FontSize = [12],
  /// FontWeight = w400
  xSmall_regular,

  /// FontSize = [11],
  /// FontWeight = w800
  xxSmall_bold,

  /// FontSize = [11],
  /// FontWeight = w500
  xxSmall_medium,

  /// FontSize = [11],
  /// FontWeight = w400
  xxSmall_regular,
}

enum _FontSizeEnum {
  xLarge,
  large,
  body,
  small,
  xSmall,
  xxSmall,
}

enum _FontWeightEnum {
  bold,
  medium,
  regular,
}
