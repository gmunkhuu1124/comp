part of utils;

extension StringTransformToWidget on String {
  Widget toWidget({
    Key? key,
    List<String>? args,
    Map<String, String>? namedArgs,
    String? gender,
    TextStyle? newStyle,
    Color? color,
    InlineSpan? textSpan,
    CustomTextStyle? style,
    StrutStyle? strutStyle,
    TextAlign? textAlign,
    TextDirection? textDirection,
    Locale? locale,
    bool? softWrap,
    TextOverflow? overflow,
    double? textScaleFactor,
    int? maxLines,
    String? semanticsLabel,
    TextWidthBasis? textWidthBasis,
    ui.TextHeightBehavior? textHeightBehavior,
  }) =>
      CustomText(
        this.tr(args: args, namedArgs: namedArgs, gender: gender),
        key: key,
        style: style,
        newStyle: newStyle,
        color: color,
        strutStyle: strutStyle,
        textAlign: textAlign,
        textDirection: textDirection,
        locale: locale,
        softWrap: softWrap,
        overflow: overflow,
        textScaleFactor: textScaleFactor,
        maxLines: maxLines,
        semanticsLabel: semanticsLabel,
        textWidthBasis: textWidthBasis,
        textHeightBehavior: textHeightBehavior,
      );
  Widget toLabel({
    Key? key,
    List<String>? args,
    Map<String, String>? namedArgs,
    String? gender,
  }) =>
      CustomLabel(
        this.tr(args: args, namedArgs: namedArgs, gender: gender),
        key: key,
      );

  String translate({
    List<String>? args,
    Map<String, String>? namedArgs,
    String? gender,
  }) =>
      this.tr(args: args, namedArgs: namedArgs, gender: gender);
}

extension DoubleTransformToWidget on double {
  CustomText toWidget({
    Key? key,
    String curCode = "mnt",
    String? pattern,
    int maxFractionLength = 2,
    List<String>? args,
    Map<String, String>? namedArgs,
    String? gender,
    TextStyle? newStyle,
    Color? color,
    InlineSpan? textSpan,
    CustomTextStyle? style,
    StrutStyle? strutStyle,
    TextAlign? textAlign,
    TextDirection? textDirection,
    Locale? locale,
    bool? softWrap,
    TextOverflow? overflow,
    double? textScaleFactor,
    int? maxLines,
    String? semanticsLabel,
    TextWidthBasis? textWidthBasis,
    ui.TextHeightBehavior? textHeightBehavior,
  }) =>
      CustomText(
        this
            .format(
              curCode: curCode,
              maxFractionLength: maxFractionLength,
              pattern: pattern,
            )
            .tr(args: args, namedArgs: namedArgs, gender: gender),
        key: key,
        style: style,
        newStyle: newStyle,
        color: color,
        strutStyle: strutStyle,
        textAlign: textAlign,
        textDirection: textDirection,
        locale: locale,
        softWrap: softWrap,
        overflow: overflow,
        textScaleFactor: textScaleFactor,
        maxLines: maxLines,
        semanticsLabel: semanticsLabel,
        textWidthBasis: textWidthBasis,
        textHeightBehavior: textHeightBehavior,
      );
}

extension ParserExtension on Object {
  double? parseDouble({
    bool? nullable = true,
  }) {
    if (this is double) return this as double?;

    String doubleStr = this.toString();
    return double.tryParse(doubleStr.replaceAll(",", "")) ?? ((nullable ?? false) ? null : 0.0);
  }

  int? parseInt({
    bool? nullable = true,
  }) {
    if (this is int) return this as int?;

    String doubleStr = this.toString();
    return int.tryParse(doubleStr) ?? ((nullable ?? false) ? null : 0);
  }

  String parseString({
    FormatType formatType = FormatType.titleCase,
  }) {
    String str = this.toString();

    str = str.trim();
    switch (formatType) {
      case FormatType.upperCase:
        str = str.toUpperCase();
        break;
      case FormatType.lowerCase:
        str = str.toLowerCase();
        break;
      default:
        str = str.toLowerCase();
        str = "${str[0].toUpperCase()}${str.substring(1)}";
    }

    return str;
  }
}

extension StringFormatterExtension on String {
  String format({
    FormatType formatType = FormatType.titleCase,
  }) {
    return Formatter.string(this, formatType: formatType);
  }
}

extension DoubleFormatterExtension on double {
  String format({String? pattern, int maxFractionLength = 2, String curCode = ""}) {
    return Formatter.currency(this, pattern: pattern, maxFractionLength: maxFractionLength, curCode: curCode);
  }
}

extension IntegerFormatterExtension on int {
  String format({String? pattern, int maxFractionLength = 2, String curCode = "MNT"}) {
    return Formatter.currency(this.toDouble(), pattern: pattern, maxFractionLength: maxFractionLength, curCode: curCode);
  }
}

// extension CameraExceptionToFailure on CameraException {
//   Failure toFailure() {
//     return Failure.danger('Error: ${this.code}\nError Message: ${this.description}');
//   }
// }
