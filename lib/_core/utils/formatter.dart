part of utils;

enum FormatType {
  titleCase,
  lowerCase,
  upperCase,
}

class Formatter {
  static String date(
    DateTime date, {
    String pattern = "yyyy-MM-dd",
  }) {
    final intl.DateFormat formatter = intl.DateFormat(pattern);
    return formatter.format(date);
  }

  static String dateTime(
    DateTime date, {
    String pattern = "yyyy-MM-dd hh:mm:ss",
  }) {
    final intl.DateFormat formatter = intl.DateFormat(pattern);
    return formatter.format(date);
  }

  static String currency(double number,
      {String? pattern, int maxFractionLength = 2, String? curCode}) {
    if (pattern != null) {
      final intl.NumberFormat formatter = intl.NumberFormat(pattern, 'ts');
      return formatter.format(number) + getCurrencySymbol(curCode);
    }

    String fractionStr = "$number".split(".")[1];
    int? fraction =
        fractionStr.length == 1 ? int.tryParse(fractionStr) ?? 0 : null;

    if (fraction != null && fraction == 0) {
      pattern = "#,###";
      final intl.NumberFormat formatter = intl.NumberFormat(pattern, 'ts');
      return formatter.format(number) + getCurrencySymbol(curCode);
    }

    String fractionPattern = List.generate(
            math.min(fractionStr.length, maxFractionLength), (_) => "#")
        .reduce((a, b) => "$a$b");
    pattern = "#,###.$fractionPattern";
    final intl.NumberFormat formatter = intl.NumberFormat(pattern, 'ts');
    return formatter.format(number) + getCurrencySymbol(curCode);
  }

  static String time(int secs) {
    const String empty = "00";
    var hour = (secs ~/ 3600).toString().padLeft(2, '0');
    var minutes = ((secs % 3600) ~/ 60).toString().padLeft(2, '0');
    var seconds = (secs % 60).toString().padLeft(2, '0');
    return "${hour != empty ? "$hour:" : ""}${minutes != empty || hour != empty ? "$minutes:" : ""}$seconds";
  }

  static String string(
    String? str, {
    FormatType formatType = FormatType.titleCase,
  }) {
    if (str == null) return "";

    str = str.trim();
    switch (formatType) {
      case FormatType.upperCase:
        str = str.toUpperCase();
        break;
      case FormatType.lowerCase:
        str = str.toLowerCase();
        break;
      case FormatType.titleCase:
        str = str.toLowerCase();
        str = "${str[0].toUpperCase()}${str.substring(1)}";
        break;
      default:
        return str;
    }

    return str;
  }

  static String getCurrencySymbol(String? curCode) {
    switch (curCode?.format(formatType: FormatType.upperCase)) {
      case "MNT":
        return "₮";
      case "EUR":
        return "€";
      case "USD":
        return "\$";
      default:
        return "";
    }
  }

  static NumberSymbols get numberSymbol => const NumberSymbols(
        NAME: "ts",
        DECIMAL_SEP: '.',
        GROUP_SEP: '\'',
        PERCENT: '%',
        ZERO_DIGIT: '0',
        PLUS_SIGN: '+',
        MINUS_SIGN: '-',
        EXP_SYMBOL: 'e',
        PERMILL: '\u2030',
        INFINITY: '\u221E',
        NAN: 'NaN',
        DECIMAL_PATTERN: '#,##0.###',
        SCIENTIFIC_PATTERN: '#E0',
        PERCENT_PATTERN: '#,##0%',
        CURRENCY_PATTERN: "\u00A4#'##0.00",
        DEF_CURRENCY_CODE: 'MNT',
      );
}
