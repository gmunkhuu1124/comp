part of utils;


class Parser {
  static String? parseString(Object? object, [bool nullable = false]) {
    if (nullable && object == null) return null;
    return object?.toString().trim() ?? "";
  }

  static double? parseDouble(Object? object, [bool nullable = false]) {
    if (nullable && object == null) return null;

    if (!nullable && object == null) return 0.0;

    double? val = double.tryParse(object!.toString().replaceAll(RegExp(r"[,']"), ""));

    return nullable ? val : (val ?? 0.0);
  }

  static int? parseInt(Object? object, [bool nullable = false]) {
    if (nullable && object == null) return null;

    if (!nullable && object == null) return 0;

    int? val = int.tryParse(object!.toString().replaceAll(RegExp(r"[,']"), ""));

    return nullable ? val : (val ?? 0);
  }

  static DateTime? parseDateTime(Object? object, [bool nullable = false]) {
    if (nullable && object == null) return null;

    if (!nullable && object == null) return DateTime(0);

    DateTime? val = DateTime.tryParse(object!.toString());

    return nullable ? val : (val ?? DateTime(0));
  }
}
