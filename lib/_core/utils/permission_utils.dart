part of utils;


class PermissionUtils {
  Future<bool> requestLocation() async {
    final permissionStatus = await Permission.location.request();

    if (permissionStatus == PermissionStatus.granted) {
      return true;
    }

    return false;
  }

  Future<bool> hasLocation() async {
    return await Permission.locationWhenInUse.serviceStatus.isEnabled;
  }
}