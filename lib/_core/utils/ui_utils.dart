part of utils;

class UIUtils {
  static callback(void Function() callback) {
    WidgetsBinding.instance!.addPostFrameCallback((_) => callback());
  }

  static double getScale(BuildContext context) {
    const uxScreenWidth = 375.0;
    return MediaQuery.of(context).size.width / uxScreenWidth;
  }

  static Future<T?> showCustomBottomSheet<T>({
    required BuildContext context,
    required Widget Function(BuildContext) builder,
  }) {
    final borderRadius = Styles.size.bsHeaderBorderRadius;
    return showModalBottomSheet<T>(
        elevation: 1,
        clipBehavior: Clip.hardEdge,
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
          topLeft: Radius.circular(borderRadius),
          topRight: Radius.circular(borderRadius),
        )),
        builder: builder);
  }

  static void hideKeyboard(BuildContext context) {
    callback(() {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
      FocusScope.of(context).requestFocus(FocusNode());
    });
  }

  static Future<void> showSuccessBottomSheet(BuildContext context,
      {String? title, String? desc, Function? onClose}) async {
    await showModalBottomSheet<bool>(
        context: context,
        elevation: 0,
        backgroundColor: Colors.transparent,
        builder: (context) => WillPopScope(onWillPop: () {
              return Future.value(true);
            }, child: LayoutBuilder(
              builder: (context, state) {
                return Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(32)),
                    padding: const EdgeInsets.symmetric(
                        vertical: 24, horizontal: 16),
                    margin: const EdgeInsets.all(10),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        FittedBox(
                          child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: R.assets.success().value,
                          ),
                        ),
                        CustomText(
                          title ?? "success".tr(),
                          style: CustomTextStyle.large_medium,
                        ),
                        AppPadding.large,
                        Text(
                          desc ??
                              "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w300,
                            color: Styles.color.txt,
                          ),
                        ),
                        AppPadding.xxLarge,
                        CustomPrimaryButton(
                          title: "Ok".toWidget(),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    ));
              },
            )));
    if (onClose != null) onClose();
  }

  static void showRegSelectorDialog(
      BuildContext context, void Function(String) onTap) {
    final _cyrillicList = getCyrillicList();
    showModalBottomSheet<String>(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext context) {
          return _DialogCard(
              child: NotificationListener<OverscrollIndicatorNotification>(
            onNotification: (overscroll) {
              overscroll.disallowGlow();
              return false;
            },
            child: GridView.builder(
              shrinkWrap: true,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 5,
                  crossAxisSpacing: 10,
                  childAspectRatio: 1,
                  mainAxisSpacing: 10),
              itemCount: _cyrillicList.length,
              itemBuilder: (context, i) {
                return GestureDetector(
                  child: Container(
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6),
                          border: Border.all(color: Color(0xFFE4E7EC))),
                      child: Text(
                        _cyrillicList[i],
                        style: TextStyle(
                          color: Color(0xFF1B2330),
                          fontWeight: FontWeight.w300,
                          fontSize: 14,
                        ),
                      )),
                  onTap: () {
                    Navigator.pop<String>(context);
                    onTap(_cyrillicList[i]);
                  },
                );
              },
            ),
          ));
        });
  }

  static List<String> getCyrillicList() {
    return <String>[
      'А',
      'Б',
      'В',
      'Г',
      'Д',
      'Е',
      'Ё',
      'Ж',
      'З',
      'И',
      'Й',
      'К',
      'Л',
      'М',
      'Н',
      'О',
      'Ө',
      'П',
      'Р',
      'С',
      'Т',
      'У',
      'Ү',
      'Ф',
      'Х',
      'Ц',
      'Ч',
      'Ш',
      'Щ',
      'Ъ',
      'Ы',
      'Ь',
      'Э',
      'Ю',
      'Я'
    ];
  }
  // static Future<void> showSuccessDialog(
  //   BuildContext context, {
  //   String? title,
  //   required String description,
  //   VoidCallback? onPressedYes,
  //   VoidCallback? onPressedNo,
  //   VoidCallback? onClose,
  // }) async {
  //   await showDialog(
  //       context: context,
  //       builder: (context) => CustomDialog(
  //             icons: [R.assets.success().value],
  //             title: title ?? "success",
  //             description: description,
  //             actions: (onPressedYes != null || onPressedNo != null)
  //                 ? [
  //                     CustomPrimaryButton(
  //                         title: CustomText("btn.yes"),
  //                         onPressed: () {
  //                           Navigator.pop(context);
  //                           if (onPressedYes != null) onPressedYes();
  //                         }),
  //                     CustomPlainButton(
  //                         title: CustomText("btn.no"),
  //                         onPressed: () {
  //                           Navigator.pop(context);
  //                           if (onPressedNo != null) onPressedNo();
  //                         })
  //                   ]
  //                 : [
  //                     CustomPrimaryButton(
  //                         title: CustomText("btn.ok"),
  //                         onPressed: () {
  //                           Navigator.pop(context);
  //                         }),
  //                   ],
  //           ));
  //   if (onClose != null) onClose();
  // }

  // static Future<bool?> showWarningDialog(
  //   BuildContext context, {
  //   String? title,
  //   required String description,
  //   String? titleYes,
  //   String? titleNo,
  //   String? titleOk,
  //   VoidCallback? onPressedYes,
  //   VoidCallback? onPressedNo,
  //   VoidCallback? onClose,
  // }) async {
  //   return showDialog<bool>(
  //       context: context,
  //       builder: (context) => CustomDialog(
  //             icons: [R.assets.warning().value],
  //             title: title ?? "warning",
  //             description: description,
  //             actions: onPressedYes != null || onPressedNo != null || titleYes != null || titleNo != null
  //                 ? [
  //                     CustomPrimaryButton(
  //                         title: CustomText(titleYes ?? "btn.yes"),
  //                         onPressed: () {
  //                           Navigator.pop(context, true);
  //                           if (onPressedYes != null) onPressedYes();
  //                         }),
  //                     CustomPlainButton(
  //                         title: CustomText(titleNo ?? "btn.no"),
  //                         onPressed: () {
  //                           Navigator.pop(context, false);
  //                           if (onPressedNo != null) onPressedNo();
  //                         })
  //                   ]
  //                 : [
  //                     CustomPrimaryButton(
  //                         title: CustomText(titleOk ?? "btn.ok"),
  //                         onPressed: () {
  //                           Navigator.pop(context);
  //                         }),
  //                   ],
  //           ));
  //   if (onClose != null) onClose();
  // }

  // static Future<void> showErrorDialog(
  //   BuildContext context, {
  //   VoidCallback? onPressedYes,
  //   VoidCallback? onPressedNo,
  //   VoidCallback? onClose,
  // }) async {
  //   await showDialog(
  //       context: context,
  //       builder: (context) => CustomDialog(
  //             icons: [R.assets.success().value],
  //             title: "Анхаар",
  //             description: "Alert with couple choices goes like this",
  //             actions: onPressedYes != null || onPressedNo != null
  //                 ? [
  //                     CustomPrimaryButton(
  //                         title: CustomText("globals.text.yes()"),
  //
  //                         // title: "yes".toWidget(),
  //                         onPressed: () {
  //                           Navigator.pop(context);
  //                           if (onPressedYes != null) onPressedYes();
  //                         }),
  //                     CustomPlainButton(
  //                         title: CustomText("globals.text.yes()"),
  //
  //                         // title: "no".toWidget(),
  //                         onPressed: () {
  //                           Navigator.pop(context);
  //                           if (onPressedNo != null) onPressedNo();
  //                         })
  //                   ]
  //                 : [
  //                     CustomPrimaryButton(
  //                         title: CustomText("globals.text.yes()"),
  //
  //                         // title: "ok".toWidget(),
  //                         onPressed: () {
  //                           Navigator.pop(context);
  //                         }),
  //                   ],
  //           ));

  //   if (onClose != null) onClose();
  // }

  // static Future<T?> showCustomDialog<T>({
  //   required BuildContext context,
  //   required Widget Function(BuildContext) builder,
  //   String? title,
  //   EdgeInsets? padding,
  //   EdgeInsets insetPadding = const EdgeInsets.symmetric(horizontal: 40.0, vertical: 24.0),
  //   VoidCallback? onPressPositive,
  //   VoidCallback? onPressNegative,
  // }) {
  //   return showDialog<T>(
  //       context: context,
  //       builder: (context) => AlertDialog(
  //             titleTextStyle: UIUtils.getTextStyleFromEnum(CustomTextStyle.large_medium),
  //             title: Center(child: title?.toWidget()),
  //             titlePadding: const EdgeInsets.all(24),
  //             contentPadding: padding ?? const EdgeInsets.symmetric(horizontal: 24),
  //             actionsPadding: const EdgeInsets.all(24),
  //             shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
  //             insetPadding: insetPadding,
  //             content: builder(context),
  //             actions: [
  //               ConstrainedBox(
  //                 constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width),
  //                 child: Column(
  //                   mainAxisAlignment: MainAxisAlignment.center,
  //                   mainAxisSize: MainAxisSize.min,
  //                   children: [
  //                     onPressPositive != null || onPressNegative != null
  //                         ? Column(
  //                             children: [
  //                               CustomPrimaryButton(
  //                                   title: "yes".toWidget(),
  //                                   onPressed: () {
  //                                     Navigator.pop(context);
  //                                     if (onPressPositive != null) onPressPositive();
  //                                   }),
  //                               CustomPrimaryButton(
  //                                   title: "no".toWidget(),
  //                                   onPressed: () {
  //                                     Navigator.pop(context);
  //                                     if (onPressNegative != null) onPressNegative();
  //                                   }),
  //                             ],
  //                           )
  //                         : CustomPrimaryButton(
  //                             title: "ok".toWidget(),
  //                             onPressed: () {
  //                               Navigator.pop(context);
  //                             }),
  //                   ],
  //                 ),
  //               )
  //             ],
  //           ));
  // }

  // static void showSnackBar({
  //   String? message,
  //   int? statusCode,
  //   Duration? duration,
  // }) {
  //   switch (statusCode) {
  //     case 0:
  //       showSuccessSnackBar(message: message ?? "globals.text.success()", duration: duration);
  //       break;
  //     case FailureLevel.low:
  //       showSimpleSnackBar(message, duration: duration);
  //       break;
  //     case FailureLevel.danger:
  //       showErrorSnackBar(message, duration: duration);
  //       break;
  //     case FailureLevel.ignore:
  //       break;
  //     default:
  //       showWarningSnackBar(message ?? "globals.text.errorOccurred()", duration: duration);
  //   }
  // }

  // static void showSuccessSnackBar({
  //   String? message,
  //   Duration? duration,
  // }) {
  //   showSimpleNotification(
  //     CustomSnackBar(
  //       color: Styles.color.success,
  //       icon: R.assets.successOutlined().value,
  //       message: message ?? "globals.text.success()",
  //     ),
  //     autoDismiss: true,
  //     duration: duration ?? Duration(seconds: 4),
  //     background: Colors.transparent,
  //     contentPadding: const EdgeInsets.all(10),
  //     elevation: 0,
  //     slideDismissDirection: DismissDirection.up,
  //     position: NotificationPosition.top,
  //   );
  // }

  // static void showSimpleSnackBar(
  //   String? message, {
  //   Duration? duration,
  // }) {
  //   showSimpleNotification(
  //     CustomSnackBar(
  //       icon: R.assets.warningOutlined().value,
  //       message: message,
  //     ),
  //     autoDismiss: true,
  //     duration: duration ?? Duration(seconds: 4),
  //     background: Colors.transparent,
  //     contentPadding: const EdgeInsets.all(10),
  //     elevation: 0,
  //     slideDismissDirection: DismissDirection.up,
  //     position: NotificationPosition.top,
  //   );
  // }

  // static void showWarningSnackBar(
  //   String message, {
  //   Duration? duration,
  // }) {
  //   showSimpleNotification(
  //     CustomSnackBar(
  //       color: Styles.color.warning,
  //       icon: R.assets.warningOutlined().value,
  //       message: message,
  //     ),
  //     autoDismiss: true,
  //     duration: duration ?? Duration(seconds: 4),
  //     background: Colors.transparent,
  //     contentPadding: const EdgeInsets.all(10),
  //     elevation: 0,
  //     slideDismissDirection: DismissDirection.up,
  //     position: NotificationPosition.top,
  //   );
  // }

  // static void showErrorSnackBar(
  //   String? message, {
  //   Duration? duration,
  // }) {
  //   showSimpleNotification(
  //     CustomSnackBar(
  //       color: Styles.color.error,
  //       icon: R.assets.warningOutlined().value,
  //       message: message,
  //     ),
  //     autoDismiss: true,
  //     duration: duration ?? Duration(seconds: Styles.size.sbDuration),
  //     background: Colors.transparent,
  //     contentPadding: const EdgeInsets.all(10),
  //     elevation: 0,
  //     slideDismissDirection: DismissDirection.up,
  //     position: NotificationPosition.top,
  //   );
  // }

  static double getLetterSpacing(double fontSize) =>
      (fontSize > 15 ? -24 : -16) * .01;

  static TextStyle? getTextStyleFromEnum(CustomTextStyle? style,
      [Color? color]) {
    if (style == null && color == null) return null;
    if (style == null) return TextStyle(color: color);
    List<String> configs = _extractStyleStr(style);
    double fontSize = _getFontSizeFromStyle(configs[0]);
    FontWeight fontWeight = _getFontWeightFromStyle(configs[1]);
    double letterSpacing = getLetterSpacing(fontSize);

    return TextStyle(
      fontSize: fontSize,
      fontWeight: fontWeight,
      letterSpacing: letterSpacing,
      color: color ?? Styles.color.primary,
    );
  }

  static List<String> _extractStyleStr(CustomTextStyle style) {
    return style.toString().split('.').last.split("_");
  }

  static double _getFontSizeFromStyle(String fontSize) {
    switch (_stringToFontSize(fontSize)) {
      case _FontSizeEnum.xLarge:
        return Styles.fontSize.xLarge;
      case _FontSizeEnum.large:
        return Styles.fontSize.large;
      case _FontSizeEnum.body:
        return Styles.fontSize.body;
      case _FontSizeEnum.small:
        return Styles.fontSize.small;
      case _FontSizeEnum.xSmall:
        return Styles.fontSize.xSmall;
      case _FontSizeEnum.xxSmall:
        return Styles.fontSize.xxSmall;
      default:
        return Styles.fontSize.body;
    }
  }

  static FontWeight _getFontWeightFromStyle(String fontWeight) {
    switch (_stringToFontWeight(fontWeight)) {
      case _FontWeightEnum.bold:
        return Styles.fontWeight.bold;
      case _FontWeightEnum.medium:
        return Styles.fontWeight.medium;
      case _FontWeightEnum.regular:
        return Styles.fontWeight.regular;
      default:
        return Styles.fontWeight.regular;
    }
  }

  static _FontSizeEnum _stringToFontSize(String fontSize) {
    return _FontSizeEnum.values
        .firstWhere((e) => e.toString().split('.').last == fontSize);
  }

  static _FontWeightEnum _stringToFontWeight(String fontWeight) {
    return _FontWeightEnum.values
        .firstWhere((e) => e.toString().split('.').last == fontWeight);
  }

  // static void showDateTimeRangePicker(BuildContext context, DateTimeRangePickerCallback onComplete) {
  //   showModalBottomSheet(
  //       context: context,
  //       isScrollControlled: true,
  //       shape: RoundedRectangleBorder(
  //         borderRadius: BorderRadius.only(
  //           topLeft: Radius.circular(13),
  //           topRight: Radius.circular(13),
  //         ),
  //       ),
  //       builder: (context) => CustomDateTimeRangePicker(onComplete: onComplete));
  // }

  // static B? Function(Failure) handleError<B>({
  //   B? Function(NWFailure)? onNWFailure,
  //   B? Function(NoInternetFailure)? onNoInternetFailure,
  //   B? Function(ServerDownFailure)? onServerDownFailure,
  //   B? Function(InternalErrorFailure)? onInternalErrorFailure,
  //   B? Function(Failure)? orElse,
  // }) {
  //   if (orElse == null)
  //     orElse = (failure) {
  //       UIUtils.showSnackBar(message: failure.message, statusCode: failure.statusCode);
  //     };

  //   return (Failure f) {
  //     if (f is NWFailure) {
  //       if (onNWFailure != null) {
  //         return onNWFailure(f);
  //       } else if (orElse != null) {
  //         return orElse(f);
  //       }
  //     } else if (f is NoInternetFailure) {
  //       if (onNoInternetFailure != null) {
  //         return onNoInternetFailure(f);
  //       } else if (orElse != null) {
  //         return orElse(f);
  //       }
  //     } else if (f is ServerDownFailure) {
  //       if (onServerDownFailure != null) {
  //         return onServerDownFailure(f);
  //       } else if (orElse != null) {
  //         return orElse(f);
  //       }
  //     } else if (f is InternalErrorFailure) {
  //       if (onInternalErrorFailure != null) {
  //         return onInternalErrorFailure(f);
  //       } else if (orElse != null) {
  //         return orElse(f);
  //       }
  //     }
  //     logError(f);
  //     UIUtils.showSnackBar(message: f.message, statusCode: f.statusCode);
  //   };
  // }

}

class _DialogCard extends StatelessWidget {
  final Widget child;

  const _DialogCard({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(10),
        child: Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(36)),
            padding: const EdgeInsets.all(30),
            width: double.infinity,
            child: child));
  }
}
