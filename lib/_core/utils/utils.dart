library utils;

import 'dart:async';
// import 'dart:io';
import 'dart:math' as math;
// import 'dart:typed_data';

import 'package:comp/_core/ui/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'dart:ui' as ui show TextHeightBehavior;
// import 'package:dartz/dartz.dart' as dartz;
// import 'package:device_info/device_info.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart' as intl;
import 'package:intl/number_symbols.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:easy_localization/easy_localization.dart' as locale;
import '../core.dart';

part 'extensions.dart';
part 'validator.dart';
part 'ui_utils.dart';
part 'enums.dart';
part 'formatter.dart';
part 'parser.dart';
part 'permission_utils.dart';
part 'device_utils.dart';