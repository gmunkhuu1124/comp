part of utils;

class Validator {
  static String? none(Object? _) => null;
  static String? required<T>(T? value) {
    if (value is List) {
      return value != null && value.length > 0 && value.toString().isNotEmpty ? null : "invalidInput".tr();
    } else {
      return value != null && value.toString().isNotEmpty ? null : "invalidInput".tr();
    }
  }

  static String? mustBeChecked(bool? value) {
    return value != null && value ? null : "mustBeChecked".tr();
  }
}

class Validation {
  static bool required(Object? value) {
    if (value == null) return false;

    return value.toString().isNotEmpty;
  }

  static bool isEmail(String value) {
    final RegExp regExp = RegExp(r"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");

    return regExp.hasMatch(value);
  }

  static bool isValidYear(int year, int minYear) {
    return year >= minYear && year <= DateTime.now().year;
  }

  static bool isPhone(String value) {
    final RegExp regExp = RegExp(r"\d{8}");

    return regExp.hasMatch(value);
  }

  static bool isRegisterNo(String value) {
    final RegExp regExp = RegExp(r"[а-яА-ЯүөёҮЁӨ]{2}\d{8}");

    return regExp.hasMatch(value);
  }

  static bool isCyrillic(String value) {
    final RegExp regExp1 = RegExp(r"^[а-яА-ЯүөёҮЁӨ]+$");
    final RegExp regExp2 = RegExp(r"^^[а-яА-ЯүөёҮЁӨ]+[\-]{1}[а-яА-ЯүөёҮЁӨ]+$");

    return regExp1.hasMatch(value) || regExp2.hasMatch(value);
  }

  static bool isPlateNo(String value) {
    final RegExp regExp1 = RegExp(r"\d{4}[а-яА-ЯүөёҮЁӨ]{3}");
    final RegExp regExp2 = RegExp(r"[а-яА-ЯүөёҮЁӨ]{3}\d{4}");

    if (regExp1.hasMatch(value) || regExp2.hasMatch(value)) return true;

    return false;
  }

  static bool isValidNumber(Object? input) {
    double? value = 0.0;

    if (input is String) {
      value = input.parseDouble();
    } else if (input is double) {
      value = input;
    } else if (input is int) {
      value = input.toDouble();
    }

    return value! > 0;
  }
}
