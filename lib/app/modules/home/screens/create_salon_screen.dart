import 'package:comp/_core/core.dart';
import 'package:comp/_core/ui/widgets/widgets.dart';
import 'package:flutter/material.dart';

class CreateSalonScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 32),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          R.assets.zzz().value,
          R.assets.chair().value,
          AppPadding.xxLarge,
          CustomText(
              'Танд одоогоор салон байхгүй байна. Та салон нээх товч дээр дарж өөрийн салоноо нээгээрэй.',
              textAlign: TextAlign.center,
              newStyle: TextStyle(
                  fontWeight: Styles.fontWeight.bold,
                  fontSize: Styles.fontSize.large)),
          AppPadding.xxLarge,
          AppPadding.xxLarge,
          CustomPrimaryButton(
            onPressed: () {},
            title: "Салон нээх".toWidget(),
          ),
        ],
      ),
    );
  }
}
