import 'package:comp/_core/core.dart';
import 'package:comp/app/modules/home/screens/create_salon_screen.dart';
import 'package:comp/app/modules/salon/screens/salon_screen.dart';
import 'package:comp/app/modules/search/screen/search_screen.dart';
import 'package:comp/app/modules/setting/screen/setting_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
// import 'package:easy_localization/easy_localization.dart' as easy;

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  List<TabBarItem> _tabbars = [
    TabBarItem(
        icon: R.assets.tabHomeSvg(color: Color(0xFF707070)).value,
        widget: SalonScreen()),
    TabBarItem(
        icon: R.assets.tabSearchSvg(color: Color(0xFF707070)).value,
        widget: SearchScreen()),
    TabBarItem(
        icon: R.assets.tabStoreSvg(color: Color(0xFF707070)).value,
        widget: CreateSalonScreen()),
    TabBarItem(
        icon: R.assets.tabSettingSvg(color: Color(0xFF707070)).value,
        widget: SettingScreen()),
  ];
  int? _selectedTab;

  int get selectedTab => _selectedTab ?? 0;

  @override
  void initState() {
    _tabController = TabController(length: _tabbars.length, vsync: this);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFFAFBFC),
      body: TabBarView(
        controller: _tabController,
        physics: NeverScrollableScrollPhysics(),
        children: _tabbars.map((e) => e.widget).toList(),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: ClipRRect(
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(14), topLeft: Radius.circular(14)),
        child: BottomAppBar(
          shape: CircularNotchedRectangle(),
          color: Colors.white,
          elevation: 0.1,
          child: Container(
              height: 81,
              color: Colors.white,
              // padding: EdgeInsets.only(top: 11),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                // children: List.generate(_tabbars.length, _buildTab),
                children: [
                  _buildTab(0, R.assets.tabHome),
                  _buildTab(1, R.assets.tabSearch),
                  _buildTab(2, R.assets.tabStore),
                  _buildTab(3, R.assets.tabUser),
                ],
              )),
        ),
      ),
    );
  }

  Widget _buildTab(int index, String assetName) {
    const size = 43.0;

    return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () => _onTab(index),
        child: Container(
          padding: EdgeInsets.all(10),
          height: size,
          width: size,
          decoration: BoxDecoration(
            color: _tabController.index == index
                ? Color(0xFF011F32).withOpacity(0.1)
                : Colors.white,
            borderRadius: BorderRadius.circular(12),
          ),
          child: SvgPicture.asset(assetName,
              height: 40,
              color: _tabController.index == index
                  ? Color(0xFF011F32)
                  : Color(0xFF707070)),
        ));
  }

  void _onTab(int index) {
    setState(() {
      _selectedTab = index;
    });
    _tabController.animateTo(selectedTab);
  }
}

class TabBarItem {
  Widget icon;
  Widget widget;

  TabBarItem({
    required this.icon,
    required this.widget,
  });
}
