import 'package:comp/_core/core.dart';
import 'package:comp/_core/ui/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ForgotScreen extends StatefulWidget {
  @override
  _ForgotScreenState createState() => _ForgotScreenState();
}

class _ForgotScreenState extends State<ForgotScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 32),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AppPadding.xLarge,
            CustomText(
              "Таны цахим хаяг",
              textAlign: TextAlign.left,
              newStyle:
                  TextStyle(fontSize: 26, fontWeight: Styles.fontWeight.bold),
              color: Styles.color.second,
            ),
            AppPadding.large,
            CustomText(
              "Бүртгэлээ баталгаажуулах 4 оронтой код хүлээн авах цахим хаягаа оруулна уу.",
              style: CustomTextStyle.body_regular,
              color: Styles.color.txt.withOpacity(0.5),
            ),
            AppPadding.xxLarge,
            CustomTextFormField(
              prefixIcon:
                  CustomIconContainer(icon: R.assets.envelopeOutlined().value),
              hintText: "Таны цахим хаяг".tr,
              validator: Validator.required,
              suffixIcon:
                  CustomIconContainer(icon: R.assets.remove().value, padding: 15,),
            ),
            AppPadding.large,
            CustomPrimaryButton(
              onPressed: () {
                 Get.toNamed('/secretWord');
              },
              title: "Үргэлжлүүлэх".toWidget(),
            ),
          ],
        ),
      ),
    );
  }
}
