import 'package:comp/_core/core.dart';
import 'package:comp/_core/ui/widgets/widgets.dart';
import 'package:comp/app/modules/home/screens/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xFFFAFBFC),
      body: SafeArea(
        child: Stack(
          fit: StackFit.expand,
          children: [
            Padding(
              padding: const EdgeInsets.all(32),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  const SizedBox(height: 90),
                  R.assets.logo().value,
                  const SizedBox(height: 90),
                  InkWell(
                    onTap: () {
                      Get.toNamed('/register');
                    },
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 16),
                      decoration: BoxDecoration(
                          border: Border.all(
                              width: 1,
                              color: Color(0xFF1877F2).withOpacity(0.3)),
                          borderRadius: BorderRadius.circular(12)),
                      height: 57,
                      child: Row(
                        children: [
                          R.assets.fb().value,
                          Expanded(
                            child: "Facebook-р бүртгүүлэх"
                                .toWidget(textAlign: TextAlign.center),
                          )
                        ],
                      ),
                    ),
                  ),
                  AppPadding.xLarge,
                  InkWell(
                    onTap: () {
                      Get.toNamed('/register');
                    },
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 16),
                      decoration: BoxDecoration(
                          border: Border.all(
                              width: 1,
                              color: Color(0xFF333333).withOpacity(0.3)),
                          borderRadius: BorderRadius.circular(12)),
                      height: 57,
                      child: Row(
                        children: [
                          R.assets.google().value,
                          Expanded(
                            child: "Google Account-р бүртгүүлэх"
                                .toWidget(textAlign: TextAlign.center),
                          )
                        ],
                      ),
                    ),
                  ),
                  Spacer(),
                  InkWell(
                    onTap: () {
                      Get.offAll(HomeScreen());
                    },
                    child: RichText(
                      text: TextSpan(
                        text: 'Надад ',
                        style: DefaultTextStyle.of(context).style,
                        children: const <TextSpan>[
                          TextSpan(
                              text: 'нэвтрэх',
                              style: TextStyle(color: Color(0xFF2B94EB))),
                          TextSpan(text: ' хаяг байгаа.!'),
                        ],
                      ),
                    ),
                  ),
                  AppPadding.xLarge,
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
