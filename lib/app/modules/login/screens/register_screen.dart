import 'package:comp/_core/core.dart';
import 'package:comp/_core/ui/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _registerController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // resizeToAvoidBottomInset: false,
      appBar: CustomAppBar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AppPadding.large,
              CustomTextFormField(
                label: "Овог".tr,
                validator: Validator.required,
              ),
              // AppPadding.medium,
              CustomTextFormField(
                label: "Нэр".tr,
                validator: Validator.required,
              ),
              CustomRegTextField(
                controller: _registerController,
                label: "Регистр",
                hintText: "",
              ),
              AppPadding.xLarge,
              CustomTextFormField(
                label: "Утасны дугаар".tr,
                validator: Validator.required,
              ),
              CustomTextFormField(
                label: "Цахим хаяг".tr,
                validator: Validator.required,
              ),
              // Spacer(),
              CustomPrimaryButton(
                onPressed: () {},
                title: "Хадгалах".toWidget(),
              ),
              AppPadding.xLarge,
            ],
          ),
        ),
      ),
    );
  }
}
