import 'package:comp/_core/core.dart';
import 'package:comp/_core/ui/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SecretWordScreen extends StatefulWidget {
  @override
  _SecretWordScreenState createState() => _SecretWordScreenState();
}

class _SecretWordScreenState extends State<SecretWordScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 32),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AppPadding.xLarge,
            CustomText(
              "Нууц үг үүсгэх".tr,
              textAlign: TextAlign.left,
              newStyle:
                  TextStyle(fontSize: 26, fontWeight: Styles.fontWeight.bold),
              color: Styles.color.second,
            ),
            AppPadding.large,
            CustomText(
              "Та нэвтрэх нууц үгээ үүсгэнэ үү.".tr,
              style: CustomTextStyle.body_regular,
              color: Styles.color.txt,
            ),
            AppPadding.xxLarge,
            CustomTextFormField(
              prefixIcon:
                  CustomIconContainer(icon: R.assets.lockOutline().value),
              hintText: "signIn.lblPassword".tr,
              validator: Validator.required,
              obscureText: true,
              suffixIcon: Icon(Icons.visibility_off),
            ),
            CustomTextFormField(
              prefixIcon:
                  CustomIconContainer(icon: R.assets.lockOutline().value),
              hintText: "signIn.lblPassword".tr,
              validator: Validator.required,
              obscureText: true,
              suffixIcon: Icon(Icons.visibility_off),
            ),
            Row(
              children: [
                CustomIconContainer(icon: R.assets.checkCircleBlack().value),
                CustomText(
                  "Нууц үг 6-8 тэмдэгт байх",
                  style: CustomTextStyle.body_regular,
                  color: Styles.color.txt,
                ),
              ],
            ),
            AppPadding.large,
            CustomPrimaryButton(
              onPressed: () {
                Get.toNamed('/termOfPolicy');
              },
              title: "Үргэлжлүүлэх".toWidget(),
            ),
          ],
        ),
      ),
    );
  }
}
