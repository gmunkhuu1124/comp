import 'package:comp/_core/core.dart';
import 'package:comp/_core/ui/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TermOfPolicyScreen extends StatefulWidget {
  @override
  _TermOfPolicyScreenState createState() => _TermOfPolicyScreenState();
}

class _TermOfPolicyScreenState extends State<TermOfPolicyScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 32),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AppPadding.xLarge,
            CustomText(
              "Үйлчилгээний нөхцөл".tr,
              textAlign: TextAlign.left,
              newStyle:
                  TextStyle(fontSize: 26, fontWeight: Styles.fontWeight.bold),
              color: Styles.color.second,
            ),
            AppPadding.large,
            Scrollbar(
              child: Container(
                height: MediaQuery.of(context).size.height * 0.7,
                child: CustomText(
                  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
                      .tr,
                  style: CustomTextStyle.body_regular,
                  color: Styles.color.txt,
                  overflow: TextOverflow.fade,
                ),
              ),
            ),
            AppPadding.xxLarge,
            Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                children: [
                  CustomCheckBox(
                    enabled: true,
                    value: false,
                    onChanged: (v) {},
                  ),
                  AppPadding.xSmall,
                  CustomText(
                    "Би аппликейшний үйлчилгээний нөхцөлийг уншиж\nтанилцсан ба хүлээн зөвшөөрч байна.",
                    style: CustomTextStyle.xSmall_regular,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
