import 'package:comp/_core/core.dart';
import 'package:comp/_core/ui/widgets/widgets.dart';
import 'package:comp/app/modules/salon/screens/tabs/about_screen.dart';
import 'package:comp/app/modules/salon/screens/tabs/gallery_screen.dart';
import 'package:comp/app/modules/salon/screens/tabs/rate_screen.dart';
import 'package:comp/app/modules/salon/screens/tabs/service_screen.dart';
import 'package:flutter/material.dart';

class SalonDetailScreen extends StatefulWidget {
  @override
  _SalonDetailScreenState createState() => _SalonDetailScreenState();
}

class _SalonDetailScreenState extends State<SalonDetailScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  int selectedIndex = 0;

  // List<Widget> _tabbars = [
  //   Tab(child: CustomText("Тухай")),
  //   Tab(child: CustomText("Үйлчилгээ")),
  //   Tab(child: CustomText("Үнэлгээ")),
  //   Tab(child: CustomText("Галлерей")),
  // ];

  List<String> _tabbars = ["Тухай", "Сэтгэгдэл", "Галлерей"];

  @override
  void initState() {
    _tabController = TabController(length: 4, vsync: this);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        physics: ScrollPhysics(),
        // shrinkWrap: true,
        children: [
          Stack(
            children: [
              Image.network(
                "https://source.unsplash.com/random",
                height: MediaQuery.of(context).size.height * 0.4,
                width: double.infinity,
                fit: BoxFit.cover,
              ),
              // Image.asset(
              //   R.assets.salonImage,
              //   height: MediaQuery.of(context).size.height * 0.4,
              //   width: double.infinity,
              //   fit: BoxFit.cover,
              // ),
              Positioned(
                top: 75,
                left: 30,
                child: InkWell(
                  onTap: () => Navigator.of(context).pop(),
                  child: Icon(
                    Icons.arrow_back,
                    color: Colors.white,
                  ),
                ),
              ),
              Positioned(
                top: 72.5,
                right: 35.5,
                child: Icon(
                  Icons.favorite_border,
                  color: Colors.white,
                ),
              ),

              Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.25),
                width: MediaQuery.of(context).size.width,
                // height: MediaQuery.of(context).size.height * 0.8,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.vertical(top: Radius.circular(24)),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(24.0),
                  child: Column(
                    children: [
                      AppPadding.xxLarge,
                      _headerCard(),
                      AppPadding.xxLarge,

                      Row(
                        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: List.generate(
                          3,
                          (index) => InkWell(
                            onTap: () {
                              setState(() {
                                selectedIndex = index;
                              });
                            },
                            child: Container(
                              margin: EdgeInsets.only(right: 10),
                              padding: EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 10),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(
                                      24), // Creates border
                                  color: selectedIndex == index
                                      ? Styles.color.primary
                                      : Styles.color.white),
                              child: Text(_tabbars[index],
                                  style: TextStyle(
                                      color: selectedIndex == index
                                          ? Styles.color.white
                                          : Styles.color.txt)),
                            ),
                          ),
                        ),
                      ),

                      // TabBar(
                      //   controller: _tabController,
                      //   isScrollable: true,
                      //   labelColor: Styles.color.white,
                      //   unselectedLabelColor: Styles.color.txt,
                      //   indicatorPadding: EdgeInsets.symmetric(),
                      //   indicator: BoxDecoration(
                      //       borderRadius:
                      //           BorderRadius.circular(24), // Creates border
                      //       color: Styles.color.primary),
                      //   tabs: _tabbars.map((e) => e).toList(),
                      // ),

                      if (selectedIndex == 0) AboutScreen(),
                      if (selectedIndex == 1) RateScreen(),
                      if (selectedIndex == 2) GalleryScreen(),
                      // ServiceScreen(),
                      // TabBarView(
                      //   controller: _tabController,
                      //   children: [
                      //     AboutScreen(),
                      //     ServiceScreen(),
                      //     RateScreen(),
                      //     GalleryScreen(),
                      //   ],
                      // ),
                      // Expanded(child: AboutScreen()),
                      // SizedBox(
                      //   height: 30,
                      //   width: double.infinity,
                      //   child: ListView.builder(
                      //     itemCount: 4,
                      //     shrinkWrap: true,
                      //     scrollDirection: Axis.horizontal,
                      //     itemBuilder: (ctx, i) => Container(
                      //       margin: EdgeInsets.only(right: 12),
                      //       padding:
                      //           EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                      //       decoration: BoxDecoration(
                      //         color: Styles.color.primary,
                      //         borderRadius: BorderRadius.circular(24),
                      //       ),
                      //       child: CustomText(
                      //         "Тухай",
                      //         newStyle: TextStyle(color: Styles.color.white),
                      //       ),
                      //     ),
                      //   ),
                      // )
                    ],
                  ),
                ),
              ),
              Positioned(
                top: 167,
                child: Image.asset(R.assets.salonDetail),
              ),
            ],
          )
        ],
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(8.0),
        child: CustomPrimaryButton(
          onPressed: () {},
          title: "Цаг захиалах".toWidget(),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  Widget _headerCard() {
    return Column(
      children: [
        Row(
          children: [
            CustomText('Soyol Wellness Center',
                newStyle: TextStyle(
                    fontWeight: Styles.fontWeight.bold,
                    fontSize: Styles.fontSize.large)),
            // _statusWidget(),
            Spacer(),
            R.assets.star().value,
            CustomText('4.3',
                newStyle: TextStyle(
                    fontWeight: Styles.fontWeight.bold,
                    fontSize: Styles.fontSize.large)),
          ],
        ),
        AppPadding.medium,
        Row(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.grey.withOpacity(0.5),
                borderRadius: BorderRadius.circular(100),
              ),
              child: R.assets.service1().value,
            ),
            AppPadding.large,
            Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.grey.withOpacity(0.5),
                borderRadius: BorderRadius.circular(100),
              ),
              child: R.assets.service2().value,
            ),
            AppPadding.large,
            Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.grey.withOpacity(0.5),
                borderRadius: BorderRadius.circular(100),
              ),
              child: R.assets.service3().value,
            ),
            AppPadding.large,
            Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.grey.withOpacity(0.5),
                borderRadius: BorderRadius.circular(100),
              ),
              child: R.assets.service4().value,
            ),
          ],
        ),
        // Row(
        //   children: [
        //     R.assets.locationPoint().value,
        //     AppPadding.xSmall,
        //     CustomText('Peace Avenue (0.04 mi) Ulaanbaatar...',
        //         newStyle: TextStyle(fontSize: Styles.fontSize.body)),
        //   ],
        // ),
        // AppPadding.small,
        // _starWidget(),
      ],
    );
  }

  Widget _statusWidget() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 21, vertical: 5),
      decoration: BoxDecoration(
        color: Color(0xFF00CF58),
        borderRadius: BorderRadius.circular(24),
      ),
      child: CustomText(
        "Open",
        newStyle: TextStyle(color: Styles.color.white),
      ),
    );
  }

  Widget _starWidget() {
    return Row(
      children: [
        ...List.generate(5, (index) => R.assets.star().value),
        AppPadding.small,
        CustomText(
          "(80)",
          newStyle: TextStyle(color: Styles.color.txt),
        )
      ],
    );
  }
}

class _TabBarItem {
  Widget title;
  Widget widget;

  _TabBarItem({
    required this.title,
    required this.widget,
  });
}
