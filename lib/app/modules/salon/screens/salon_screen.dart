import 'package:comp/_core/core.dart';
import 'package:comp/_core/ui/widgets/widgets.dart';
import 'package:comp/app/modules/home/screens/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SalonScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: Styles.padding.xLarge,
                vertical: Styles.padding.large),
            child: Column(
              children: [
                Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: Image.network(
                        "https://source.unsplash.com/user/erondu",
                        height: 40,
                        width: 40,
                        fit: BoxFit.cover,
                      ),
                    ),
                    AppPadding.small,
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CustomText("Өдрийн мэнд"),
                        CustomText('Галмандах',
                            newStyle:
                                TextStyle(fontWeight: Styles.fontWeight.bold)),
                      ],
                    ),
                    Spacer(),
                    Row(
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 12, vertical: 10),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(12)),
                          child: R.assets.bell().value,
                        ),
                        AppPadding.small,
                        Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 12, vertical: 10),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(12)),
                          child: R.assets.calendar().value,
                        ),
                      ],
                    )
                  ],
                ),
                AppPadding.xxxLarge,
                Row(
                  children: [
                    CustomText('Миний захиалга',
                        newStyle: TextStyle(
                            fontWeight: Styles.fontWeight.bold,
                            fontSize: Styles.fontSize.large)),
                    Spacer(),
                    CustomText('Өнөөдөр',
                        newStyle: TextStyle(color: Styles.color.txt)),
                  ],
                ),
                AppPadding.xLarge,
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 19),
                  decoration: BoxDecoration(
                      // color: Color(0xFFF4F6F8),
                      borderRadius: BorderRadius.circular(12),
                      gradient: new LinearGradient(
                          colors: [
                            const Color(0xFF9BA6B8),
                            const Color(0xFF011F32),
                          ],
                          begin: const FractionalOffset(0.0, 0.0),
                          end: const FractionalOffset(1.0, 0.0),
                          stops: [0.0, 1.0],
                          tileMode: TileMode.clamp)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      R.assets.smile().value,
                      AppPadding.small,
                      CustomText('Эхний захиалгаа өгье',
                          newStyle: TextStyle(
                              color: Styles.color.white,
                              fontWeight: FontWeight.bold)),
                    ],
                  ),
                ),
                AppPadding.xxxLarge,
                Row(
                  children: [
                    CustomText('Ойр салон',
                        newStyle: TextStyle(
                            fontWeight: Styles.fontWeight.bold,
                            fontSize: Styles.fontSize.large)),
                    Spacer(),
                    CustomText('Бүгдийг харах',
                        newStyle: TextStyle(color: Styles.color.second)),
                  ],
                ),
                AppPadding.small,
                // Container(
                //   height: 270,
                //   width: double.infinity,
                //   decoration: BoxDecoration(
                //     borderRadius: BorderRadius.circular(24),
                //     image: DecorationImage(
                //         image: NetworkImage("https://source.unsplash.com/random"),
                //         fit: BoxFit.cover),
                //   ),
                //   child: Column(
                //     mainAxisAlignment: MainAxisAlignment.end,
                //     children: [
                //       Container(
                //         width: double.infinity,
                //         decoration: BoxDecoration(
                //             color: Color(0xFFF4F6F8),
                //             borderRadius:
                //                 BorderRadius.vertical(bottom: Radius.circular(24))),
                //         child: Padding(
                //           padding: const EdgeInsets.all(16.0),
                //           child: Column(
                //             children: [
                //               Row(
                //                 crossAxisAlignment: CrossAxisAlignment.center,
                //                 children: [
                //                   R.assets.greenRound().value,
                //                   AppPadding.xSmall,
                //                   CustomText('Soyol Wellness Center',
                //                       newStyle: TextStyle(
                //                           fontWeight: Styles.fontWeight.bold,
                //                           fontSize: Styles.fontSize.body)),
                //                   Spacer(),
                //                   R.assets.star().value,
                //                   CustomText('4.2',
                //                       newStyle: TextStyle(
                //                           fontWeight: Styles.fontWeight.bold,
                //                           fontSize: Styles.fontSize.body)),
                //                 ],
                //               ),
                //               AppPadding.medium,
                //               Row(
                //                 children: [
                //                   R.assets.locationPoint().value,
                //                   AppPadding.xSmall,
                //                   CustomText('Peace Avenue (0.04 mi) Ulaanbaatar...',
                //                       newStyle:
                //                           TextStyle(fontSize: Styles.fontSize.body)),
                //                 ],
                //               ),
                //               AppPadding.xLarge,
                //               CustomPrimaryButton(
                //                 onPressed: () {
                //                   // Navigator.pushReplacement(context, route: HomeScreen());
                //                   Get.offAll(HomeScreen());
                //                 },
                //                 title: "Захиалах".toWidget(),
                //               ),
                //             ],
                //           ),
                //         ),
                //       )
                //     ],
                //   ),
                // )
              ],
            ),
          ),
          SizedBox(
            height: 160,
            width: double.infinity,
            child: ListView.builder(
              itemCount: 2,
              shrinkWrap: true,
              padding: EdgeInsets.only(left: 24, right: 8),
              scrollDirection: Axis.horizontal,
              itemBuilder: (ctx, i) => Container(
                margin: EdgeInsets.only(right: 16),
                width: MediaQuery.of(context).size.width - 48,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(24),
                  image: DecorationImage(
                      image: NetworkImage("https://source.unsplash.com/random"),
                      fit: BoxFit.cover),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    // ClipRRect(
                    //   borderRadius:
                    //       BorderRadius.vertical(top: Radius.circular(24)),
                    //   child: Image.network(
                    //     "https://source.unsplash.com/random",
                    //     height: 117,
                    //     // width: double.infinity,
                    //     // width: MediaQuery.of(context).size.width,
                    //     fit: BoxFit.cover,
                    //   ),
                    // ),
                    Container(
                      decoration: BoxDecoration(
                        color: Color(0xFFF4F6F8),
                        borderRadius:
                            BorderRadius.vertical(bottom: Radius.circular(24)),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                // R.assets.greenRound().value,
                                // AppPadding.xSmall,
                                InkWell(
                                  onTap: () => Get.toNamed("/salonDetail"),
                                  child: CustomText('Soyol Wellness Center',
                                      newStyle: TextStyle(
                                          fontWeight: Styles.fontWeight.bold,
                                          fontSize: Styles.fontSize.body)),
                                ),
                                Spacer(),
                                // R.assets.star().value,
                                CustomText('09:00 - 20:00',
                                    newStyle: TextStyle(
                                        fontSize: 10)),
                              ],
                            ),
                            AppPadding.medium,
                            Row(
                              children: [
                                // R.assets.locationPoint().value,
                                // AppPadding.xSmall,
                                CustomText(
                                    'Peace Avenue (0.04 mi) Ulaanbaatar...',
                                    newStyle: TextStyle(
                                        fontSize: Styles.fontSize.xSmall)),
                              ],
                            ),
                            // AppPadding.xLarge,
                            // CustomPrimaryButton(
                            //   icon: R.assets.clock().value,
                            //   onPressed: () {
                            //     // Get.offAll(HomeScreen());
                            //   },
                            //   title: "Захиалах".toWidget(),
                            // ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
