import 'package:comp/_core/core.dart';
import 'package:comp/_core/ui/widgets/widgets.dart';
import 'package:comp/app/modules/home/screens/create_salon_screen.dart';
import 'package:flutter/material.dart';

class AboutScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: ScrollPhysics(),
      shrinkWrap: true,
      // crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        AppPadding.xxLarge,
        CustomText(
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an u ....",
          maxLines: 4,
          overflow: TextOverflow.ellipsis,
          newStyle: TextStyle(
            color: Styles.color.txt,
          ),
        ),
        AppPadding.xxLarge,
        _scheduleWidget(),
        AppPadding.xxLarge,
        _contactWidget(),
        AppPadding.xxLarge,
        ..._addressWidget(),
        AppPadding.xxLarge,
        _otherBranchWidget(),
        const SizedBox(height: 54),
      ],
    );
  }

  Widget _scheduleWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomText('Цагийн хуваарь',
            newStyle: TextStyle(
                fontWeight: Styles.fontWeight.bold,
                fontSize: Styles.fontSize.large)),
        AppPadding.large,
        Row(
          children: [
            CustomText('Даваа - Баасан',
                newStyle: TextStyle(color: Styles.color.txt)),
            AppPadding.xxxLarge,
            CustomText('08:00 - 22:00',
                newStyle: TextStyle(fontWeight: Styles.fontWeight.bold)),
          ],
        ),
        Row(
          children: [
            CustomText('Бямба - Ням',
                newStyle: TextStyle(color: Styles.color.txt)),
            const SizedBox(width: 54),
            CustomText('10:00 - 22:00',
                newStyle: TextStyle(fontWeight: Styles.fontWeight.bold)),
          ],
        ),
      ],
    );
  }

  Widget _contactWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomText('Холбогдох',
            newStyle: TextStyle(
                fontWeight: Styles.fontWeight.bold,
                fontSize: Styles.fontSize.large)),
        AppPadding.large,
        CustomText(
          '+976 9910-6574',
          newStyle: TextStyle(
              color: Styles.color.second, fontSize: Styles.fontSize.large),
        ),
      ],
    );
  }

  _addressWidget() {
    return [
      CustomText('Хаяг',
          newStyle: TextStyle(
              fontWeight: Styles.fontWeight.bold,
              fontSize: Styles.fontSize.large)),
      AppPadding.large,
      CustomText(
        'Улаанбаатар хот, Баянгол дүүрэг, 3-4-р хороолол, Номин худалдааны төвийн урд, PCmall худалдааны төвийн хажууд.',
        maxLines: 3,
        overflow: TextOverflow.ellipsis,
      ),
      AppPadding.large,
      Container(
        height: 120,
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.grey,
          borderRadius: BorderRadius.circular(24),
        ),
        child: Center(child: Text("test")),
      )
    ];
  }

  Widget _otherBranchWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomText('Бусад салбарууд',
            newStyle: TextStyle(
                fontWeight: Styles.fontWeight.bold,
                fontSize: Styles.fontSize.large)),
        AppPadding.xLarge,
        Row(
          children: [
            CircleAvatar(
              backgroundImage:
                  NetworkImage("https://source.unsplash.com/random"),
            ),
            AppPadding.medium,
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText("Matrix salon",
                    newStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: Styles.fontSize.body)),
                CustomText("Peace Avenue (0.04 mi) Ulaanbaatar...")
              ],
            ),
            Spacer(),
            R.assets.angleRight().value,
            const SizedBox(width: 18)
          ],
        ),
        AppPadding.large,
        Row(
          children: [
            CircleAvatar(
              backgroundImage:
                  NetworkImage("https://source.unsplash.com/random"),
            ),
            AppPadding.medium,
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText("Matrix salon",
                    newStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: Styles.fontSize.body)),
                CustomText("Peace Avenue (0.04 mi) Ulaanbaatar...")
              ],
            ),
            Spacer(),
            R.assets.angleRight().value,
            const SizedBox(width: 18)
          ],
        ),
        AppPadding.large,
        Row(
          children: [
            CircleAvatar(
              backgroundImage:
                  NetworkImage("https://source.unsplash.com/random"),
            ),
            AppPadding.medium,
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText("Matrix salon",
                    newStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: Styles.fontSize.body)),
                CustomText("Peace Avenue (0.04 mi) Ulaanbaatar...")
              ],
            ),
            Spacer(),
            R.assets.angleRight().value,
            const SizedBox(width: 18)
          ],
        ),
      ],
    );
  }
}
