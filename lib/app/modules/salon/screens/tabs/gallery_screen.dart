import 'package:comp/_core/core.dart';
import 'package:flutter/material.dart';

class GalleryScreen extends StatefulWidget {
  @override
  _GalleryScreenState createState() => _GalleryScreenState();
}

class _GalleryScreenState extends State<GalleryScreen> {
  @override
  Widget build(BuildContext context) {
    return GridView.count(
      physics: ScrollPhysics(),
      shrinkWrap: true,
      padding: EdgeInsets.only(top: Styles.padding.xxLarge),
      crossAxisCount: 3,
      crossAxisSpacing: 1,
      mainAxisSpacing: 1,
      children: [
        ...List.generate(
            100,
            (index) => Image.network("https://source.unsplash.com/random",
                fit: BoxFit.cover))
      ],
    );
  }
}
