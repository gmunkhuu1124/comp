import 'package:comp/_core/core.dart';
import 'package:comp/_core/ui/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RateScreen extends StatefulWidget {
  @override
  _RateScreenState createState() => _RateScreenState();
}

class _RateScreenState extends State<RateScreen> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: ScrollPhysics(),
      shrinkWrap: true,
      children: [
        AppPadding.xxLarge,
        CustomTextFormField(
          hintText: "Сэтгэгдлээ бичиж үлдээнэ үү".tr,
          validator: Validator.required,
          obscureText: true,
          // suffixIcon: Image.asset(R.assets.sendMessage),
          // suffixIcon: Image(image: AssetImage(R.assets.sendMessage)),
        ),
        AppPadding.xxLarge,
        CustomText('Сэтгэгдлүүд (2)',
            newStyle: TextStyle(
                fontWeight: Styles.fontWeight.bold,
                fontSize: Styles.fontSize.large)),
        AppPadding.xxLarge,
        _buildCommentWidget(),
        AppPadding.xxLarge,
        _buildCommentWidget(),
        AppPadding.xxxLarge,
        AppPadding.xxxLarge,
      ],
    );
  }

  Column _buildCommentWidget() {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CircleAvatar(
              backgroundImage:
                  NetworkImage("https://source.unsplash.com/random"),
              maxRadius: 20,
            ),
            AppPadding.small,
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText(
                  "Bat Gal",
                  newStyle: TextStyle(fontWeight: Styles.fontWeight.bold),
                ),
                AppPadding.small,
                Row(
                  children: [
                    ...List.generate(5, (index) => R.assets.star().value)
                  ],
                )
              ],
            ),
            Spacer(),
            CustomText(
              "2 хоногын өмнө",
              newStyle: TextStyle(color: Styles.color.txt),
            ),
          ],
        ),
        const SizedBox(height: 19),
        CustomText(
          "Надад тохирох үсний засалт болон хэрхэн арчлах талаар сайн зөвлөгөө өгсөн. Үнийн хувьд боломжийн үйлчилгээ сайтай газар байна. Танай хамт олонд амжилт хүсье.",
          maxLines: 4,
          overflow: TextOverflow.ellipsis,
        )
      ],
    );
  }
}
