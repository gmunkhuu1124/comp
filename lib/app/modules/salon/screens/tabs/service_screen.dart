import 'package:comp/_core/core.dart';
import 'package:comp/_core/ui/widgets/widgets.dart';
import 'package:flutter/material.dart';

class ServiceScreen extends StatefulWidget {
  @override
  _ServiceScreenState createState() => _ServiceScreenState();
}

class _ServiceScreenState extends State<ServiceScreen> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: ScrollPhysics(),
      shrinkWrap: true,
      children: [
        AppPadding.xxLarge,
        Container(
          padding: EdgeInsets.symmetric(vertical: 15, horizontal: 16),
          decoration: BoxDecoration(
              color: Color(0xFFF4F6F8),
              borderRadius: BorderRadius.circular(12)),
          child: Row(
            children: [
              CustomText("Бүгд", newStyle: TextStyle(color: Styles.color.txt)),
              Spacer(),
              Icon(Icons.keyboard_arrow_down),
              AppPadding.large,
            ],
          ),
        ),
        AppPadding.large,
        Container(
          padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
          decoration: BoxDecoration(
              color: Color(0xFFF4F6F8),
              border: Border.all(color: Styles.color.second, width: 1),
              borderRadius: BorderRadius.circular(12)),
          child: Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  CustomText(
                    "Эрэгтэй энгийн засалт",
                    newStyle: TextStyle(fontWeight: Styles.fontWeight.bold),
                  ),
                  AppPadding.xSmall,
                  CustomText(
                    "20 мин",
                    newStyle: TextStyle(
                        color: Styles.color.txt,
                        fontSize: Styles.fontSize.xSmall),
                  ),
                ],
              ),
              Spacer(),
              R.assets.checkBoxWarning().value
            ],
          ),
        ),
        AppPadding.large,
        Container(
          padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
          decoration: BoxDecoration(
              color: Color(0xFFF4F6F8),
              borderRadius: BorderRadius.circular(12)),
          child: Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  CustomText(
                    "Эрэгтэй энгийн засалт",
                    newStyle: TextStyle(fontWeight: Styles.fontWeight.bold),
                  ),
                  AppPadding.xSmall,
                  CustomText(
                    "20 мин",
                    newStyle: TextStyle(
                        color: Styles.color.txt,
                        fontSize: Styles.fontSize.xSmall),
                  ),
                ],
              ),
              Spacer(),
              Container(
                height: 20,
                width: 20,
                decoration: BoxDecoration(
                    border: Border.all(
                      color: Color(0xFF808F99),
                    ),
                    borderRadius: BorderRadius.circular(12)),
              )
            ],
          ),
        ),
        AppPadding.large,
        Container(
          padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
          decoration: BoxDecoration(
              color: Color(0xFFF4F6F8),
              borderRadius: BorderRadius.circular(12)),
          child: Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  CustomText(
                    "Эрэгтэй энгийн засалт",
                    newStyle: TextStyle(fontWeight: Styles.fontWeight.bold),
                  ),
                  AppPadding.xSmall,
                  CustomText(
                    "20 мин",
                    newStyle: TextStyle(
                        color: Styles.color.txt,
                        fontSize: Styles.fontSize.xSmall),
                  ),
                ],
              ),
              Spacer(),
              Container(
                height: 20,
                width: 20,
                decoration: BoxDecoration(
                    border: Border.all(
                      color: Color(0xFF808F99),
                    ),
                    borderRadius: BorderRadius.circular(12)),
              )
            ],
          ),
        ),
        const SizedBox(height: 42),
        CustomPrimaryButton(
          onPressed: () {
            // Get.offAll(HomeScreen());
          },
          title: "Захиалга өгөх".toWidget(),
        ),
        const SizedBox(height: 54),
      ],
    );
  }
}
