import 'dart:async';

import 'package:comp/_core/core.dart';
import 'package:comp/_core/ui/styles/styles.dart';
import 'package:comp/_core/ui/widgets/widgets.dart';
import 'package:comp/app/modules/home/screens/create_salon_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  Completer<GoogleMapController> _controller = Completer();

  @override
  void initState() {
    super.initState();
  }

  double zoomVal = 5.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: <Widget>[
          _buildGoogleMap(context),
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              height: 109,
              padding: EdgeInsets.only(left: 32, right: 32, top: 24),
              decoration: BoxDecoration(
                color: Styles.color.background,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(24),
                  bottomRight: Radius.circular(24),
                ),
              ),
              child: CustomTextFormField(
                prefixIcon: Icon(Icons.search),
                hintText: "Салон хайх",
                validator: Validator.required,
                obscureText: true,
                suffixIcon: Icon(Icons.filter_list),
              ),
            ),
          ),
          // _zoomminusfunction(),
          // _zoomplusfunction(),
          _buildContainer(),
        ],
      ),
    );
  }

  Widget _zoomminusfunction() {
    return Align(
      alignment: Alignment.topLeft,
      child: IconButton(
          icon: Icon(Icons.search, color: Color(0xff6200ee)),
          onPressed: () {
            zoomVal--;
            _minus(zoomVal);
          }),
    );
  }

  Widget _zoomplusfunction() {
    return Align(
      alignment: Alignment.topRight,
      child: IconButton(
          icon: Icon(Icons.search, color: Color(0xff6200ee)),
          onPressed: () {
            zoomVal++;
            _plus(zoomVal);
          }),
    );
  }

  Future<void> _minus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(40.712776, -74.005974), zoom: zoomVal)));
  }

  Future<void> _plus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(40.712776, -74.005974), zoom: zoomVal)));
  }

  Widget _buildContainer() {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Container(
        height: 112,
        margin: EdgeInsets.only(bottom: 24),
        child: ListView.builder(
          itemCount: 3,
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24, right: 24),
          scrollDirection: Axis.horizontal,
          itemBuilder: (ctx, i) => Container(
            margin: EdgeInsets.only(right: 24),
            padding: EdgeInsets.all(16),
            width: MediaQuery.of(context).size.width - 65,
            decoration: BoxDecoration(
              color: Color(0xFFF4F6F8),
              borderRadius: BorderRadius.circular(24),
            ),
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(24),
                  child: Image.network(
                    "https://source.unsplash.com/random",
                    height: 80,
                    width: 80,
                    fit: BoxFit.cover,
                  ),
                ),
                AppPadding.large,
                Expanded(
                  child: Column(
                    children: [
                      Row(
                        children: [
                          R.assets.greenRound().value,
                          AppPadding.xSmall,
                          InkWell(
                            onTap: () => Get.toNamed("/salonDetail"),
                            child: CustomText('Soyol Wellness Center',
                                newStyle: TextStyle(
                                    fontWeight: Styles.fontWeight.bold,
                                    fontSize: Styles.fontSize.body)),
                          ),
                        ],
                      ),
                      AppPadding.medium,
                      Row(
                        children: [
                          R.assets.locationPoint().value,
                          AppPadding.xSmall,
                          Expanded(
                            child: CustomText(
                                'Peace Avenue (0.04 mi) Ulaanbaatar...',
                                overflow: TextOverflow.ellipsis,
                                newStyle:
                                    TextStyle(fontSize: Styles.fontSize.small)),
                          ),
                        ],
                      ),
                      AppPadding.medium,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          _starWidget(),
                          CustomText(
                            "3км",
                            newStyle: TextStyle(
                              fontWeight: Styles.fontWeight.bold,
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _starWidget() {
    return Row(
      children: [
        ...List.generate(5, (index) => R.assets.star().value),
        AppPadding.small,
        CustomText(
          "(80)",
          newStyle: TextStyle(color: Styles.color.txt),
        )
      ],
    );
  }

  Widget _boxes(String _image, double lat, double long, String restaurantName) {
    return GestureDetector(
      onTap: () {
        _gotoLocation(lat, long);
      },
      child: Container(
        width: MediaQuery.of(context).size.width - 48,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(24),
          color: Colors.white,
        ),
        child: Row(
          // mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                width: 80,
                height: 80,
                child: ClipRRect(
                  borderRadius: new BorderRadius.circular(24.0),
                  child: Image(
                    fit: BoxFit.cover,
                    image: NetworkImage(_image),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16),
              child: myDetailsContainer1(restaurantName),
            ),
          ],
        ),
      ),
    );
  }

  Widget myDetailsContainer1(String restaurantName) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          children: [
            R.assets.greenRound().value,
            AppPadding.xSmall,
            Text(
              "Matrix salon",
              style: TextStyle(
                  color: Styles.color.primary,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
        Row(
          children: [
            R.assets.locationPoint().value,
            AppPadding.xSmall,
            CustomText('Peace Avenue (0.04 mi) Ulaanbaatar...',
                newStyle: TextStyle(fontSize: 12)),
          ],
        ),
      ],
    );
  }

  Widget _buildGoogleMap(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: GoogleMap(
        mapType: MapType.normal,
        initialCameraPosition:
            CameraPosition(target: LatLng(40.712776, -74.005974), zoom: 12),
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
        markers: {
          newyork1Marker,
          newyork2Marker,
          newyork3Marker,
          gramercyMarker,
          bernardinMarker,
          blueMarker
        },
      ),
    );
  }

  Future<void> _gotoLocation(double lat, double long) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: LatLng(lat, long),
      zoom: 15,
      tilt: 50.0,
      bearing: 45.0,
    )));
  }
}

Marker gramercyMarker = Marker(
  markerId: MarkerId('gramercy'),
  position: LatLng(40.738380, -73.988426),
  infoWindow: InfoWindow(title: 'Gramercy Tavern'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);

Marker bernardinMarker = Marker(
  markerId: MarkerId('bernardin'),
  position: LatLng(40.761421, -73.981667),
  infoWindow: InfoWindow(title: 'Le Bernardin'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker blueMarker = Marker(
  markerId: MarkerId('bluehill'),
  position: LatLng(40.732128, -73.999619),
  infoWindow: InfoWindow(title: 'Blue Hill'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueYellow,
  ),
);

//New York Marker

Marker newyork1Marker = Marker(
  markerId: MarkerId('newyork1'),
  position: LatLng(40.742451, -74.005959),
  infoWindow: InfoWindow(title: 'Los Tacos'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker newyork2Marker = Marker(
  markerId: MarkerId('newyork2'),
  position: LatLng(40.729640, -73.983510),
  infoWindow: InfoWindow(title: 'Tree Bistro'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker newyork3Marker = Marker(
  markerId: MarkerId('newyork3'),
  position: LatLng(40.719109, -74.000183),
  infoWindow: InfoWindow(title: 'Le Coucou'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
