import 'package:comp/_core/core.dart';
import 'package:comp/_core/ui/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SettingScreen extends StatefulWidget {
  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  List<_MenuItem> _menuItems = [
    _MenuItem(
      icon: R.assets.userPlus().value,
      title: "Үйлчилгээний нөхцөл",
      onTap: () => {},
    ),
    _MenuItem(
      icon: R.assets.userPlus().value,
      title: "Төлбөрийн картууд",
      subTitle: "Банкны карт холбох, шинэчлэх",
      onTap: () => {},
    ),
    _MenuItem(
      icon: R.assets.xxx().value,
      title: "Гарах",
      titleColor: Color(0xFFFF3100),
      onTap: () => {},
      isShowRightIcon: false,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: Styles.padding.xLarge, vertical: Styles.padding.large),
      child: Column(
        children: [
          AppPadding.large,
          InkWell(
            onTap: () => Get.toNamed('/userScreen'),
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(12),
                  child: Image.network(
                    "https://source.unsplash.com/user/erondu",
                    height: 87,
                    width: 87,
                    fit: BoxFit.cover,
                  ),
                ),
                AppPadding.large,
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomText('Батдорж Галмандах',
                        newStyle: TextStyle(
                          fontSize: Styles.fontSize.large,
                          fontWeight: Styles.fontWeight.bold,
                        )),
                    CustomText("User1234@mail.com",
                        newStyle: TextStyle(
                          color: Styles.color.txt,
                        )),
                  ],
                ),
                Spacer(),
                R.assets.angleRight().value,
                const SizedBox(width: 18),
              ],
            ),
          ),
          AppPadding.xLarge,
          ..._menuItems.map((e) => _buildItem(e))
        ],
      ),
    );
  }

  Widget _buildItem(_MenuItem e) {
    return GestureDetector(
      onTap: () => e.onTap,
      child: Container(
        margin: EdgeInsets.only(bottom: 24),
        padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 16),
        decoration: BoxDecoration(
          color: Color(0xFFF4F6F8),
          borderRadius: BorderRadius.circular(12),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            e.icon,
            const SizedBox(width: 19),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText(
                  e.title,
                  newStyle: TextStyle(
                      fontSize: Styles.fontSize.body,
                      color: e.titleColor ?? Styles.color.primary,
                      fontWeight: FontWeight.bold),
                ),
                e.subTitle != null
                    ? CustomText(
                        e.subTitle,
                        newStyle: TextStyle(fontSize: 12),
                      )
                    : Container()
              ],
            ),
            Spacer(),
            if (e.isShowRightIcon) R.assets.angleRight().value,
            const SizedBox(width: 8),
          ],
        ),
      ),
    );
  }
}

class _MenuItem {
  Widget icon;
  String title;
  String? subTitle;
  Color? titleColor;
  Function onTap;
  bool isShowRightIcon;

  _MenuItem({
    required this.icon,
    required this.title,
    this.subTitle,
    this.titleColor,
    required this.onTap,
    this.isShowRightIcon = true,
  });
}
