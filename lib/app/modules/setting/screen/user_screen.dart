import 'package:comp/_core/core.dart';
import 'package:comp/_core/ui/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UserScreen extends StatefulWidget {
  @override
  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 16),
          child: Column(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: Image.network(
                  "https://source.unsplash.com/user/erondu",
                  height: 87,
                  width: 87,
                  fit: BoxFit.cover,
                ),
              ),
              AppPadding.large,
              CustomTextFormField(
                label: "Овог",
                hintText: "Овог".tr,
                validator: Validator.required,
              ),
              AppPadding.large,
              CustomTextFormField(
                label: "Нэр",
                hintText: "Нэр".tr,
                validator: Validator.required,
              ),
              AppPadding.large,
              CustomTextFormField(
                label: "Овог",
                hintText: "Овог".tr,
                validator: Validator.required,
              ),
              AppPadding.large,
              CustomTextFormField(
                label: "Овог",
                hintText: "Овог".tr,
                validator: Validator.required,
              ),
              AppPadding.large,
              CustomPrimaryButton(
                onPressed: () {},
                title: "Хадгалах".toWidget(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
