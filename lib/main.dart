import 'package:comp/app/modules/login/screens/forgot_screen.dart';
import 'package:comp/app/modules/login/screens/login_screen.dart';
import 'package:comp/app/modules/login/screens/register_screen.dart';
import 'package:comp/app/modules/salon/screens/salon_detail_screen.dart';
import 'package:comp/app/modules/setting/screen/user_screen.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '_core/core.dart';
import 'app/modules/home/screens/home_screen.dart';
import 'app/modules/login/screens/secret_word_screen.dart';
import 'app/modules/login/screens/term_of_policy_screen.dart';

void main() {
  runApp(
    EasyLocalization(
      supportedLocales: [Locale('mn', 'MN'), Locale('en', 'US')],
      path: 'assets/locales',
      fallbackLocale: Locale('en', 'US'),
      startLocale: Locale('mn', 'MN'),
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      themeMode: ThemeMode.light,
      theme: ThemeData.light().copyWith(
        scaffoldBackgroundColor: Styles.color.background,
        appBarTheme: AppBarTheme(
          systemOverlayStyle: Styles.systemUiOverlay,
          color: Styles.color.primary,
        ),
      ),
      initialRoute: "/",
      routes: {
        '/': (context) => LoginScreen(),
        '/register': (context) => RegisterScreen(),
        '/forgot': (context) => ForgotScreen(),
        '/secretWord': (context) => SecretWordScreen(),
        '/termOfPolicy': (context) => TermOfPolicyScreen(),
        '/homeScreen': (context) => HomeScreen(),
        '/userScreen': (context) => UserScreen(),
        '/salonDetail': (context) => SalonDetailScreen(),
      },
      builder: (context, child) => SafeArea(
          child: Material(
              color: Theme.of(context).scaffoldBackgroundColor, child: child)),
    );
  }
}
